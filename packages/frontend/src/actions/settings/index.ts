import { SettingsThemeActionsInterface } from './theme';

export * from './theme';

export type SettingsActionsInterface =
  SettingsThemeActionsInterface;
