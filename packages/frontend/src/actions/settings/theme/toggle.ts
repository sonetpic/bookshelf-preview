import { Action } from 'redux';
import { SettingsThemeActionTypes } from '.';

export type SettingsThemeToggleActionInterface = Action<
  SettingsThemeActionTypes.toggle
>;

export const toggleTheme = (): SettingsThemeToggleActionInterface => {
  return {
    type: SettingsThemeActionTypes.toggle,
  };
};
