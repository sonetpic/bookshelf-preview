import { Action } from 'redux';
import { SettingsThemeActionTypes } from '.';
import { PrefersColorSchemeEnum } from '../../../helpers/detect-prefers-theme';

export interface SettingsThemeSetActionInterface
extends Action<SettingsThemeActionTypes.set> {
  theme: PrefersColorSchemeEnum;
}

export const setTheme = (
  theme: PrefersColorSchemeEnum,
): SettingsThemeSetActionInterface => {
  return {
    type: SettingsThemeActionTypes.set,
    theme,
  };
};
