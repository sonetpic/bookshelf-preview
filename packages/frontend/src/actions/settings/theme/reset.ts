import { Action } from 'redux';
import { SettingsThemeActionTypes } from '.';

export type SettingsThemeResetActionInterface = Action<
  SettingsThemeActionTypes.reset
>;

export const resetTheme = (): SettingsThemeResetActionInterface => {
  return {
    type: SettingsThemeActionTypes.reset,
  };
};
