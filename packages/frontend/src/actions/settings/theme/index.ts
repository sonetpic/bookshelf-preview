import { SettingsThemeResetActionInterface } from './reset';
import { SettingsThemeSetActionInterface } from './set';
import { SettingsThemeToggleActionInterface } from './toggle';

export * from './set';
export * from './toggle';

export enum SettingsThemeActionTypes {
  set = 'SET_THEME',
  toggle = 'TOGGLE_THEME',
  reset = 'RESET_THEME',
}

export type SettingsThemeActionsInterface =
  SettingsThemeSetActionInterface |
  SettingsThemeToggleActionInterface |
  SettingsThemeResetActionInterface;
