import { AuthorWithBookIdsDto } from '@said-m/bookshelf-common/dist/entities';
import { AuthorsCollectionActionTypes } from '.';
import { API, ApiActionInterface, ApiActionsInterface, ApiFailureActionInterface, ApiRequestActionInterface, ApiSuccessActionInterface } from '../../middleware/api';
import { authorsSchema } from '../../middleware/api/schemas';

export enum AuthorsCollectionUpdateAsyncActionTypes {
  request = 'UPDATE_AUTHOR_REQUEST',
  success = 'UPDATE_AUTHOR_SUCCESS',
  failure = 'UPDATE_AUTHOR_FAILURE',
}

export type AuthorsCollectionUpdateRequestInterface =
  Partial<AuthorWithBookIdsDto>;
export type AuthorsCollectionUpdateResponseInterface =
  Partial<AuthorWithBookIdsDto>;

export type AuthorsCollectionUpdateRequestActionInterface =
  ApiRequestActionInterface<
    AuthorsCollectionUpdateAsyncActionTypes.request
  >;

export type AuthorsCollectionUpdateSuccessActionInterface =
  ApiSuccessActionInterface<
    AuthorsCollectionUpdateAsyncActionTypes.success,
    AuthorsCollectionUpdateResponseInterface
  >;

export type AuthorsCollectionUpdateFailureActionInterface =
  ApiFailureActionInterface<
    AuthorsCollectionUpdateAsyncActionTypes.failure
  >;

export type AuthorsCollectionUpdateActionsInterface =
  ApiActionsInterface<
    AuthorsCollectionUpdateRequestActionInterface,
    AuthorsCollectionUpdateSuccessActionInterface,
    AuthorsCollectionUpdateFailureActionInterface
  >;

export const updateAuthor = (
  author: AuthorsCollectionUpdateRequestInterface,
): ApiActionInterface<
  AuthorsCollectionUpdateRequestInterface
> => {
  return {
    type: AuthorsCollectionActionTypes.update,
    [API]: {
      types: AuthorsCollectionUpdateAsyncActionTypes,
      endpoint: '/api/authors/update',
      schema: authorsSchema,
      payload: author,
    },
  };
};
