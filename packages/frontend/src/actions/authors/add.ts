import { AuthorBasicDto, AuthorDto } from '@said-m/bookshelf-common/dist/entities';
import { AuthorsCollectionActionTypes } from '.';
import { API, ApiActionInterface, ApiActionsInterface, ApiFailureActionInterface, ApiRequestActionInterface, ApiSuccessActionInterface } from '../../middleware/api';
import { authorsSchema } from '../../middleware/api/schemas';

export enum AuthorsCollectionAddAsyncActionTypes {
  request = 'ADD_AUTHOR_REQUEST',
  success = 'ADD_AUTHOR_SUCCESS',
  failure = 'ADD_AUTHOR_FAILURE',
}

export type AuthorsCollectionAddRequestActionInterface =
  ApiRequestActionInterface<
    AuthorsCollectionAddAsyncActionTypes.request
  >;

export type AuthorsCollectionAddSuccessActionInterface =
  ApiSuccessActionInterface<
    AuthorsCollectionAddAsyncActionTypes.success,
    AuthorDto
  >;

export type AuthorsCollectionAddFailureActionInterface =
  ApiFailureActionInterface<
    AuthorsCollectionAddAsyncActionTypes.failure
  >;

export type AuthorsCollectionAddActionsInterface =
  ApiActionsInterface<
    AuthorsCollectionAddRequestActionInterface,
    AuthorsCollectionAddSuccessActionInterface,
    AuthorsCollectionAddFailureActionInterface
  >;

export type AuthorsAddActionInterface = ApiActionInterface<
  AuthorBasicDto,
  // TODO: нужно уметь типизировать тип триггер-экшна и его потомков
  AuthorsCollectionAddAsyncActionTypes |
  AuthorsCollectionActionTypes
>;

export const addAuthor = (
  author: AuthorBasicDto,
): AuthorsAddActionInterface => {
  return {
    type: AuthorsCollectionActionTypes.add,
    [API]: {
      types: AuthorsCollectionAddAsyncActionTypes,
      endpoint: '/api/authors/add',
      schema: authorsSchema,
      payload: author,
    },
  };
};
