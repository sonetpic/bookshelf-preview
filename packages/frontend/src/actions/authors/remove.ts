import { AuthorDto } from '@said-m/bookshelf-common/dist/entities';
import { CastArrayInterface } from '@said-m/common/dist/interfaces';
import { castArray } from 'lodash';
import { AuthorsCollectionActionTypes } from '.';
import { API, ApiActionInterface, ApiActionsInterface, ApiFailureActionInterface, ApiRequestActionInterface, ApiSuccessActionInterface } from '../../middleware/api';
import { authorsSchema } from '../../middleware/api/schemas';

export enum AuthorsCollectionRemoveAsyncActionTypes {
  request = 'REMOVE_AUTHOR_REQUEST',
  success = 'REMOVE_AUTHOR_SUCCESS',
  failure = 'REMOVE_AUTHOR_FAILURE',
}

export type AuthorsCollectionRemoveResponseInterface =
  Array<AuthorDto['id']>;

export type AuthorsCollectionRemoveRequestActionInterface =
  ApiRequestActionInterface<
    AuthorsCollectionRemoveAsyncActionTypes.request
  >;

export type AuthorsCollectionRemoveSuccessActionInterface =
  ApiSuccessActionInterface<
    AuthorsCollectionRemoveAsyncActionTypes.success,
    AuthorsCollectionRemoveResponseInterface
  >;

export type AuthorsCollectionRemoveFailureActionInterface =
  ApiFailureActionInterface<
    AuthorsCollectionRemoveAsyncActionTypes.failure
  >;

export type AuthorsCollectionRemoveActionsInterface =
  ApiActionsInterface<
    AuthorsCollectionRemoveRequestActionInterface,
    AuthorsCollectionRemoveSuccessActionInterface,
    AuthorsCollectionRemoveFailureActionInterface
  >;

type RemoveAuthorsInputInterface = CastArrayInterface<
  AuthorDto['id']
>;

export const removeAuthors = (
  authorIds: RemoveAuthorsInputInterface,
): ApiActionInterface<
  AuthorsCollectionRemoveResponseInterface
> => {
  return {
    type: AuthorsCollectionActionTypes.remove,
    [API]: {
      types: AuthorsCollectionRemoveAsyncActionTypes,
      endpoint: '/api/author/remove',
      schema: authorsSchema,
      payload: castArray(authorIds),
    },
  };
};
