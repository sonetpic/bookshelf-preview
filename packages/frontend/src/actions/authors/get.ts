import { AuthorDto } from '@said-m/bookshelf-common/dist/entities';
import { CastArrayInterface } from '@said-m/common/dist/interfaces';
import { castArray, compact } from 'lodash';
import { Dispatch } from 'react';
import { AuthorsCollectionActionTypes } from '.';
import { isExpiredData } from '../../helpers';
import { API, ApiActionInterface, ApiActionsInterface, ApiFailureActionInterface, ApiRequestActionInterface, ApiSuccessActionInterface } from '../../middleware/api';
import { authorsSchema } from '../../middleware/api/schemas';
import { RootStateInterface } from '../../reducers';

export enum AuthorsCollectionGetAsyncActionTypes {
  request = 'GET_AUTHOR_REQUEST',
  success = 'GET_AUTHOR_SUCCESS',
  failure = 'GET_AUTHOR_FAILURE',
}

export type AuthorsCollectionGetRequestActionInterface =
  ApiRequestActionInterface<
    AuthorsCollectionGetAsyncActionTypes.request
  >;

export type AuthorsCollectionGetSuccessActionInterface =
  ApiSuccessActionInterface<
    AuthorsCollectionGetAsyncActionTypes.success,
    Array<AuthorDto>
  >;

export type AuthorsCollectionGetFailureActionInterface =
  ApiFailureActionInterface<
    AuthorsCollectionGetAsyncActionTypes.failure
  >;

export type AuthorsCollectionGetActionsInterface =
  ApiActionsInterface<
    AuthorsCollectionGetRequestActionInterface,
    AuthorsCollectionGetSuccessActionInterface,
    AuthorsCollectionGetFailureActionInterface
  >;

export const getAuthors = (
  ids?: CastArrayInterface<number>,
): ApiActionInterface<
  Array<number>,
  // TODO: нужно уметь типизировать тип триггер-экшна и его потомков
  AuthorsCollectionGetAsyncActionTypes |
  AuthorsCollectionActionTypes
> => {
  const specifiedIds = compact(castArray(ids));

  return {
    type: AuthorsCollectionActionTypes.get,
    [API]: {
      types: AuthorsCollectionGetAsyncActionTypes,
      endpoint: '/api/authors/get',
      schema: authorsSchema,
      payload: specifiedIds,
      isPartial: specifiedIds.length > 0,
    },
  };
};

const shouldGetAuthors = (
  {
    authors,
  }: RootStateInterface,
): boolean => {
  if (!authors.data) {
    return true;
  } else if (authors.isLoading) {
    return false;
  } else {
    return isExpiredData(authors.receivedAt) ||
      !!authors.error;
  }
};

export const getAuthorsIfNeeded = () => {
  return (
    dispatch: Dispatch<ApiActionInterface>,
    getState: () => RootStateInterface,
  ) => {
    if (shouldGetAuthors(getState())) {
      dispatch(getAuthors());
    }
  };
};
