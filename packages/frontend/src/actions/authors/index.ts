import { AuthorsCollectionAddActionsInterface } from './add';
import { AuthorsCollectionGetActionsInterface } from './get';
import { AuthorsCollectionRemoveActionsInterface } from './remove';
import { AuthorsCollectionUpdateActionsInterface } from './update';

export * from './add';
export * from './get';
export * from './remove';
export * from './update';

export enum AuthorsCollectionActionTypes {
  get = 'GET_AUTHOR',
  add = 'ADD_AUTHOR',
  update = 'UPDATE_AUTHOR',
  remove = 'REMOVE_AUTHOR',
}

export type AuthorsCollectionActionsInterface =
  AuthorsCollectionGetActionsInterface |
  AuthorsCollectionAddActionsInterface |
  AuthorsCollectionUpdateActionsInterface |
  AuthorsCollectionRemoveActionsInterface;

export type AuthorsActionsInterface = AuthorsCollectionActionsInterface;
