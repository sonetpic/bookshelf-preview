import { BooksCollectionAddActionsInterface, BooksCollectionGetActionsInterface, BooksCollectionRemoveActionsInterface, BooksCollectionUpdateActionsInterface } from './collection';
import { setSort, toggleSort } from './sort';

export * from './collection';
export * from './sort';

export type BooksCollectionActionsInterface =
  BooksCollectionGetActionsInterface |
  BooksCollectionAddActionsInterface |
  BooksCollectionUpdateActionsInterface |
  BooksCollectionRemoveActionsInterface;

export type BooksSortActionsInterface =
  ReturnType<typeof setSort> |
  ReturnType<typeof toggleSort>;

export type BooksActionsInterface =
  BooksCollectionActionsInterface |
  BooksSortActionsInterface;
