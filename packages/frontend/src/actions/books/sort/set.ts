import { BooksSortActionTypes, BooksSortSetActionInterface, BooksSortTypes } from '.';

export const setSort = (
  sort: BooksSortTypes,
): BooksSortSetActionInterface => {
  return {
    type: BooksSortActionTypes.set,
    sort,
  };
};
