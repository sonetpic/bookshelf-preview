import { BooksSortActionTypes, BooksSortToggleActionInterface } from '.';

export const toggleSort = (): BooksSortToggleActionInterface => {
  return {
    type: BooksSortActionTypes.toggle,
  };
};
