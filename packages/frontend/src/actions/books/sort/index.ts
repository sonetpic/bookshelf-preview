import { Action } from 'redux';

export * from './set';
export * from './toggle';

export enum BooksSortActionTypes {
  set = 'SET_BOOK_SORT',
  toggle = 'TOGGLE_BOOK_SORT',
}

export enum BooksSortTypes {
  title = 'BY_TITLE',
  year = 'BY_YEAR',
}

export interface BooksSortSetActionInterface
extends Action<BooksSortActionTypes.set> {
  sort: BooksSortTypes;
}

export type BooksSortToggleActionInterface = Action<
  BooksSortActionTypes.toggle
>;
