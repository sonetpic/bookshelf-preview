import { BookDto } from '@said-m/bookshelf-common/dist/entities';
import { CastArrayInterface } from '@said-m/common/dist/interfaces';
import { castArray } from 'lodash';
import { BooksCollectionActionTypes } from '.';
import { API, ApiActionInterface, ApiActionsInterface, ApiFailureActionInterface, ApiRequestActionInterface, ApiSuccessActionInterface } from '../../../middleware/api';
import { booksSchema } from '../../../middleware/api/schemas';

export enum BooksCollectionRemoveAsyncActionTypes {
  request = 'REMOVE_BOOK_REQUEST',
  success = 'REMOVE_BOOK_SUCCESS',
  failure = 'REMOVE_BOOK_FAILURE',
}

export type BooksCollectionRemoveResponseInterface =
  Array<BookDto['id']>;

export type BooksCollectionRemoveRequestActionInterface =
  ApiRequestActionInterface<
    BooksCollectionRemoveAsyncActionTypes.request
  >;

export type BooksCollectionRemoveSuccessActionInterface =
  ApiSuccessActionInterface<
    BooksCollectionRemoveAsyncActionTypes.success,
    BooksCollectionRemoveResponseInterface
  >;

export type BooksCollectionRemoveFailureActionInterface =
  ApiFailureActionInterface<
    BooksCollectionRemoveAsyncActionTypes.failure
  >;

export type BooksCollectionRemoveActionsInterface =
  ApiActionsInterface<
    BooksCollectionRemoveRequestActionInterface,
    BooksCollectionRemoveSuccessActionInterface,
    BooksCollectionRemoveFailureActionInterface
  >;

type RemoveBooksInputInterface = CastArrayInterface<
  BookDto['id']
>;

export const removeBooks = (
  bookIds: RemoveBooksInputInterface,
): ApiActionInterface<
  BooksCollectionRemoveResponseInterface
> => {
  return {
    type: BooksCollectionActionTypes.remove,
    [API]: {
      types: BooksCollectionRemoveAsyncActionTypes,
      endpoint: '/api/books/remove',
      schema: booksSchema,
      payload: castArray(bookIds),
    },
  };
};
