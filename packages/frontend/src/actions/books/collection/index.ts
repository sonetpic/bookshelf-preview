
export * from './add';
export * from './get';
export * from './remove';
export * from './update';

export enum BooksCollectionActionTypes {
  get = 'GET_BOOK',
  add = 'ADD_BOOK',
  update = 'UPDATE_BOOK',
  remove = 'REMOVE_BOOK',
}
