import { Book } from '@said-m/bookshelf-common/dist/entities';
import { BooksCollectionActionTypes } from '.';
import { API, ApiActionInterface, ApiActionsInterface, ApiFailureActionInterface, ApiRequestActionInterface, ApiSuccessActionInterface } from '../../../middleware/api';
import { booksSchema } from '../../../middleware/api/schemas';

export enum BooksCollectionUpdateAsyncActionTypes {
  request = 'UPDATE_BOOK_REQUEST',
  success = 'UPDATE_BOOK_SUCCESS',
  failure = 'UPDATE_BOOK_FAILURE',
}

export type BooksCollectionUpdateRequestInterface =
  Partial<Book>;
export type BooksCollectionUpdateResponseInterface =
  Partial<Book>;

export type BooksCollectionUpdateRequestActionInterface =
  ApiRequestActionInterface<
    BooksCollectionUpdateAsyncActionTypes.request
  >;

export type BooksCollectionUpdateSuccessActionInterface =
  ApiSuccessActionInterface<
    BooksCollectionUpdateAsyncActionTypes.success,
    BooksCollectionUpdateResponseInterface
  >;

export type BooksCollectionUpdateFailureActionInterface =
  ApiFailureActionInterface<
    BooksCollectionUpdateAsyncActionTypes.failure
  >;

export type BooksCollectionUpdateActionsInterface =
  ApiActionsInterface<
    BooksCollectionUpdateRequestActionInterface,
    BooksCollectionUpdateSuccessActionInterface,
    BooksCollectionUpdateFailureActionInterface
  >;

export const updateBook = (
  book: Book,
): ApiActionInterface<
  BooksCollectionUpdateRequestInterface
> => {
  return {
    type: BooksCollectionActionTypes.update,
    [API]: {
      types: BooksCollectionUpdateAsyncActionTypes,
      endpoint: '/api/books/update',
      schema: booksSchema,
      payload: book,
    },
  };
};
