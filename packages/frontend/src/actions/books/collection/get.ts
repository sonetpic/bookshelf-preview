import { Book } from '@said-m/bookshelf-common/dist/entities';
import { Dispatch } from 'react';
import { BooksCollectionActionTypes } from '.';
import { isExpiredData } from '../../../helpers';
import { API, ApiActionInterface, ApiActionsInterface, ApiFailureActionInterface, ApiRequestActionInterface, ApiSuccessActionInterface } from '../../../middleware/api';
import { booksSchema } from '../../../middleware/api/schemas';
import { RootStateInterface } from '../../../reducers';

export enum BooksCollectionGetAsyncActionTypes {
  request = 'GET_BOOK_REQUEST',
  success = 'GET_BOOK_SUCCESS',
  failure = 'GET_BOOK_FAILURE',
}

export type BooksCollectionGetRequestActionInterface =
  ApiRequestActionInterface<
    BooksCollectionGetAsyncActionTypes.request
  >;

export type BooksCollectionGetSuccessActionInterface =
  ApiSuccessActionInterface<
    BooksCollectionGetAsyncActionTypes.success,
    Array<Book>
  >;

export type BooksCollectionGetFailureActionInterface =
  ApiFailureActionInterface<
    BooksCollectionGetAsyncActionTypes.failure
  >;

export type BooksCollectionGetActionsInterface =
  ApiActionsInterface<
    BooksCollectionGetRequestActionInterface,
    BooksCollectionGetSuccessActionInterface,
    BooksCollectionGetFailureActionInterface
  >;

export const getBooks = (): ApiActionInterface => {
  return {
    type: BooksCollectionActionTypes.get,
    [API]: {
      types: BooksCollectionGetAsyncActionTypes,
      endpoint: '/api/books/get',
      schema: booksSchema,
      settings: {
        relations: ['authors'],
      },
    },
  };
};

const shouldGetBooks = (
  {
    books: { collection },
  }: RootStateInterface,
): boolean => {
  if (!collection.data) {
    return true;
  } else if (collection.isLoading) {
    return false;
  } else {
    return isExpiredData(collection.receivedAt) ||
      !!collection.error;
  }
};

export const getBooksIfNeeded = () => {
  return (
    dispatch: Dispatch<ApiActionInterface>,
    getState: () => RootStateInterface,
  ) => {
    if (shouldGetBooks(getState())) {
      dispatch(getBooks());
    }
  };
};
