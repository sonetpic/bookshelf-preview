import { Book, BookBasicDto } from '@said-m/bookshelf-common/dist/entities';
import { BooksCollectionActionTypes } from '.';
import { API, ApiActionInterface, ApiActionsInterface, ApiFailureActionInterface, ApiRequestActionInterface, ApiSuccessActionInterface } from '../../../middleware/api';
import { booksSchema } from '../../../middleware/api/schemas';

export enum BooksCollectionAddAsyncActionTypes {
  request = 'ADD_BOOK_REQUEST',
  success = 'ADD_BOOK_SUCCESS',
  failure = 'ADD_BOOK_FAILURE',
}

export type BooksCollectionAddRequestActionInterface =
  ApiRequestActionInterface<
    BooksCollectionAddAsyncActionTypes.request
  >;

export type BooksCollectionAddSuccessActionInterface =
  ApiSuccessActionInterface<
    BooksCollectionAddAsyncActionTypes.success,
    Book
  >;

export type BooksCollectionAddFailureActionInterface =
  ApiFailureActionInterface<
    BooksCollectionAddAsyncActionTypes.failure
  >;

export type BooksCollectionAddActionsInterface =
  ApiActionsInterface<
    BooksCollectionAddRequestActionInterface,
    BooksCollectionAddSuccessActionInterface,
    BooksCollectionAddFailureActionInterface
  >;

export const addBook = (
  book: BookBasicDto,
): ApiActionInterface<
  BookBasicDto
> => {
  return {
    type: BooksCollectionActionTypes.add,
    [API]: {
      types: BooksCollectionAddAsyncActionTypes,
      endpoint: '/api/books/add',
      schema: booksSchema,
      payload: book,
    },
  };
};
