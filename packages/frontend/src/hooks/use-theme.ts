import { useEffect } from 'react';
import { PrefersColorSchemeEnum } from '../helpers/detect-prefers-theme';

export default function useTheme(
  theme: PrefersColorSchemeEnum,
) {
  useEffect(
    () => {
      document.body.dataset.theme = theme;

      return () => {
        document.body.removeAttribute('data-theme');
      };
    },
    [theme],
  );
}
