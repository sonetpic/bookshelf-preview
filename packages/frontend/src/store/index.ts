import { applyMiddleware, createStore, Middleware } from 'redux';
import logger from 'redux-logger';
import reduxThunk from 'redux-thunk';
import { apiMiddleware } from '../middleware/api';
import { rootReducer } from '../reducers';

const common: Array<Middleware> = [
  reduxThunk, apiMiddleware,
];

const prod: Array<Middleware> = [];

const dev: Array<Middleware> = [
  logger,
];

export const store = createStore(
  rootReducer,
  applyMiddleware(
    ...common,
    ...(
      process.env.NODE_ENV === 'production'
        ? prod
        : dev
    ),
  ),
);
