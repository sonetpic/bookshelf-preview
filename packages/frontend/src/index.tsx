import 'focus-visible';
import React from 'react';
import ReactDOM from 'react-dom';
import { Root } from './layouts/Root';

ReactDOM.render(
  <Root />,
  document.getElementById('root'),
);
