import { ObjectInterface } from '@said-m/common/dist/interfaces';
import { Form, FormikConfig } from 'formik';
import React from 'react';
import { Alerts } from '../Alerts';
import { Input } from '../Input';
import styles from './Form.module.scss';

interface FormCreatorPropsInterface {
  title?: React.ReactNode;
  fields: Array<{
    name: string;
    title: string;
  }>;
  submit: string;
  isViewMode?: boolean;
}

export const formCreator = (
  {
    title,
    fields,
    submit,
    isViewMode,
  }: FormCreatorPropsInterface,
): FormikConfig<
  // tslint:disable-next-line: no-any
  ObjectInterface<any>
>['render'] => {
  return (formProps) => {
    const {
      isSubmitting,
      dirty,
      isValid,
      error,
    } = formProps;

    const fieldList = fields.map(
      thisField => (
        <Input
          key={thisField.name}
          name={thisField.name}
          title={thisField.title}
          formikProps={formProps}
          isViewMode={isViewMode}
        />
      ),
    );

    const errorEl = error
      ? (
        <Alerts
          type='error'
          title={error.message}
          message={error.description}
        />
      )
      : undefined;

    const submitEl = !isViewMode
      ? (
        <button
          type='submit'
          disabled={!isValid || !dirty || isSubmitting}
          className={styles.submit}
        >
          {submit}
        </button>
      )
      : undefined;

    return (
      <Form className={isSubmitting ? styles.loading : undefined}>
        {title}

        {/* TODO: организовать вью-онли режим: */}
        {/* <fieldset disabled={true}></fieldset> */}

        {fieldList}

        {/* TODO: компонент для вывода ошибок */}

        {errorEl}

        {submitEl}
      </Form>
    );
  };
};
