import React from 'react';
import styles from './Wrap.module.scss';

export interface WrapPropsInterface {
  classNames?: Array<string>;
}

export const Wrap: React.FC<WrapPropsInterface> = ({
  children,
  classNames = [],
}) => {
  return (
    <section className={`${styles.wrap} ${classNames.join( )}`}>
      {children}
    </section>
  );
};
