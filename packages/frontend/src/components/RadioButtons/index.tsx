import React from 'react';
import styles from './RadioButtons.module.scss';

export interface RadioButtonsItemInterface<Value> {
  title: string;
  value: Value;
  onClick: () => void;
}

export type RadioButtonsPropsIsActiveInterface<Value> =
  (value: Value) => boolean;

interface RadioButtonsPropsInterface<Value> {
  buttons: Array<RadioButtonsItemInterface<Value>>;
  isActive: RadioButtonsPropsIsActiveInterface<Value>;
}

export class RadioButtons<Value>
extends React.Component<
  RadioButtonsPropsInterface<Value>
> {
  render() {
    const buttonListEl = this.props.buttons.map(
      (thisButton, thisButtonIndex) => {
        const isActive = this.props.isActive(thisButton.value);

        return (
          <button
            key={thisButtonIndex}
            onClick={!isActive ? thisButton.onClick : undefined}
            className={`${styles.button} ${isActive ? 'active' : ''}`}
          >
            {thisButton.title}
          </button>
        );
      },
    );

    return (
      <span className={styles.wrap}>
        {buttonListEl}
      </span>
    );
  }
}
