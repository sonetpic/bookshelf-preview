import React from 'react';
import styles from './Book.module.scss';

interface BookPropsInterface {
  image?: string;
  title: string;
  subtitle: string;
  description: React.ReactNode;
}

export const Book: React.FC<BookPropsInterface> = ({
  image,
  title,
  subtitle,
  description,
}) => {
  return (
    <article
      tabIndex={0}
      className={styles.wrap}
    >
      <img
        src={image}
        alt={`${title} // ${subtitle}`}
        className={styles.image}
      />
      <div className={styles.content}>
        <header>
          <h2 className={styles.subtitle}>
            {subtitle}
          </h2>
          <h1 className={styles.title}>
            {title}
          </h1>
        </header>

        <div className={styles.description}>
          {description}
        </div>
      </div>
    </article>
  );
};
