import React, { MouseEventHandler } from 'react';
import styles from './Book.module.scss';

interface BookPlaceholderPropsInterface {
  emoji: string;
  text: string;
  onClick?: MouseEventHandler;
}

export const BookPlaceholder: React.FC<BookPlaceholderPropsInterface> = ({
  emoji,
  text,
  onClick,
}) => {
  return (
    <article
      tabIndex={0}
      className={styles.wrap}
      onClick={onClick}
    >
      <div className={styles.placeholder}>
        <p>{emoji}</p>
        <p className={styles.title}>{text}</p>
      </div>
    </article>
  );
};
