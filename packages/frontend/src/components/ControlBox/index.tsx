import React from 'react';
import hasProperty from 'ts-has-property';
import styles from './ControlBox.module.scss';

interface ControlBoxPropsInterface {
  main: React.ReactNode;
  side: React.ReactNode;
}

export const ControlBox: React.FC<ControlBoxPropsInterface> = ({
  main,
  side,
}) => {
  const mainEl = React.Children.map(main, child => {
    if (!hasProperty(child, 'props')) {
      return;
    }

    return React.cloneElement(
      child,
      {
        className: `${child.props.className} ${styles.wrap}`,
      },
      <span className={styles.main}>{child.props.children}</span>,
      <span className={styles.side}>{side}</span>,
    );
  });

  return (
    <>
      {mainEl}
    </>
  );
};
