import React from 'react';
import { NavLink } from 'react-router-dom';
import styles from './Navigation.module.scss';

export interface NavigationPropsItemInterface {
  to: string;
  title: string;
}

interface NavigationPropsInterface {
  items: Array<NavigationPropsItemInterface>;
  className?: string;
}

export const Navigation: React.FC<
  NavigationPropsInterface
> = ({
  items,
  className,
}) => {
  const listItemsEl = items.map(
    thisItem => (
      <li
        key={thisItem.to}
        className={styles.item}
      >
        <NavLink
          exact={true}
          to={thisItem.to}
          className={styles.link}
        >
          {thisItem.title}
        </NavLink>
      </li>
    ),
  );

  return (
    <nav className={`${styles.wrap} ${className ? className : ''}`}>
      <ul className={styles.list}>
        {listItemsEl}
      </ul>
    </nav>
  );
};
