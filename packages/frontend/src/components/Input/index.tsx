import { ObjectInterface } from '@said-m/common/dist/interfaces';
import { ErrorMessage, Field, FormikProps } from 'formik';
import React from 'react';
import styles from './Input.module.scss';

interface InputPropsInterface {
  name: string;
  title: string;
  formikProps: FormikProps<
    // tslint:disable-next-line: no-any
    ObjectInterface<any>
  >;
  isViewMode?: boolean;
}

export const Input: React.FC<InputPropsInterface> = ({
  name,
  title,
  formikProps,
  isViewMode = false,
}) => {
  const error = formikProps.errors[name];
  const touched = formikProps.touched[name];

  const labelClassNames = [
    styles.label,
    !!(error && touched) ? styles.invalid : undefined,
  ].join(' ');

  return (
    <label className={labelClassNames}>
      <span className={styles.title}>{title}</span>
      <Field
        className={styles.input}
        name={name}
        readOnly={isViewMode}
      />
      <span className={styles.error}>
        <ErrorMessage name={name} />
      </span>
    </label>
  );
};
