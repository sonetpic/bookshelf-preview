import React from 'react';
import { Link } from 'react-router-dom';
import { ControlBox } from '../ControlBox';
import styles from './Author.module.scss';

export interface AuthorPropsInterface {
  title: string;
  to?: Partial<Record<
    'main' |
    'view' |
    'edit',
    string
  >>;
}

export const Author: React.FC<AuthorPropsInterface> = ({
  title,
  to = {},
}) => {
  const titleEl = (
    <h3 className={styles.title}>
      {title}
    </h3>
  );

  const viewEl = to.view && (
    <Link
      to={to.view}
      className={styles.view}
    >
      🔍
    </Link>
  );

  const editEl = to.edit && (
    <Link
      to={to.edit}
      className={styles.edit}
    >
      ✏️
    </Link>
  );

  const actionsEl = editEl && (
    <span className={styles.actions}>
      {viewEl}
      {editEl}
    </span>
  );

  const wrapEl = (
    <article className={styles.wrap}>
      <ControlBox
        main={titleEl}
        side={actionsEl}
      />
    </article>
  );

  return (
    (
      to.main && (
        <Link
          to={to.main}
          className={styles.link}
        >
          {wrapEl}
        </Link>
      )
    ) || wrapEl
  );
};
