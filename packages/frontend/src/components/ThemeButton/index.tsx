import React from 'react';
import { PrefersColorSchemeEnum } from '../../helpers/detect-prefers-theme';

export interface ThemeButtonPropsInterface
extends React.ButtonHTMLAttributes<HTMLButtonElement> {
  theme: PrefersColorSchemeEnum;
}

export const ThemeButton: React.FC<
  ThemeButtonPropsInterface
> = ({
  theme,
  children,
  // tslint:disable-next-line: trailing-comma
  ...otherProps
}) => {
  return (
    <button {...otherProps}>
      {theme === PrefersColorSchemeEnum.dark ? '☀️' : '🌑'}
    </button>
  );
};
