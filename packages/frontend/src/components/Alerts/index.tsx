import React from 'react';
import styles from './Alerts.module.scss';

export const AlertsTypes = {
  error: styles.error,
  warn: styles.warn,
  info: styles.info,
  success: styles.success,
  default: styles.default,
} as const;

interface AlertsPropsInterface {
  type: keyof typeof AlertsTypes;
  title: string;
  message: string;
}

export const Alerts: React.FC<AlertsPropsInterface> = ({
  type,
  title,
  message,
}) => {
  return (
    <div className={`${styles.wrap} ${AlertsTypes[type]}`}>
      <h4 className={styles.title}>{title}</h4>
      <p className={styles.message}>{message}</p>
    </div>
  );
};
