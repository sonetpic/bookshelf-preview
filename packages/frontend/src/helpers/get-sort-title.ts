import { BooksSortTypes } from '../actions';

export const getBooksSortTitle = (
  sort: BooksSortTypes,
): string => {
  switch (sort) {
    case BooksSortTypes.title:
      return '🔠 По названию';
    case BooksSortTypes.year:
      return '📆 По году публикации';
  }
};
