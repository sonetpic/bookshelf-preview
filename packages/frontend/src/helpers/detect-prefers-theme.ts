import { isEnumItem } from '@said-m/common';

export enum PrefersColorSchemeEnum {
  no = 'no-preference',
  light = 'light',
  dark = 'dark',
}

export const sessionTheme = 'theme';

const mediaThemeLight = createColorSchemeMedia(PrefersColorSchemeEnum.light);
const mediaThemeDark = createColorSchemeMedia(PrefersColorSchemeEnum.dark);

export const detectPrefersTheme = (): PrefersColorSchemeEnum => {
  if (!window.matchMedia) {
    return PrefersColorSchemeEnum.no;
  }

  return (
    (
      window.matchMedia(mediaThemeLight).matches &&
      PrefersColorSchemeEnum.light
    ) || (
      window.matchMedia(mediaThemeDark).matches &&
      PrefersColorSchemeEnum.dark
    ) ||
    PrefersColorSchemeEnum.no
  );
};

export const getSessionTheme = (): Exclude<
  PrefersColorSchemeEnum,
  PrefersColorSchemeEnum.no
> | null => {
  const value: string = sessionStorage.getItem(sessionTheme) ||
    PrefersColorSchemeEnum.no;

  return (
    isEnumItem(
      value,
      PrefersColorSchemeEnum,
    ) &&
    value !== 'no'
  )
    ? PrefersColorSchemeEnum[value]
    : null;
};

function createColorSchemeMedia(
  scheme: PrefersColorSchemeEnum,
) {
  return `(prefers-color-scheme: ${scheme})`;
}
