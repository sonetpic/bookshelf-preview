
const EXPIRATION_TIME = 1000 * 60 * 2;

export const isExpiredData = (time: number): boolean => {
  return Date.now() - time > EXPIRATION_TIME;
};
