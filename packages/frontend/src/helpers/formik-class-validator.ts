import { ClassType, transformAndValidate } from 'class-transformer-validator';
import { ValidationError } from 'class-validator';

interface ErrorRecordInterface {
  [key: string]: string | ErrorRecordInterface;
}

function convertErrorToFormikFormat(errors: Array<ValidationError>) {
  const result: ErrorRecordInterface = {};

  for (const error of Array.from(errors)) {
    result[error.property] = Object.values(error.constraints)[0];

    if (error.children.length) {
      result[error.property] = convertErrorToFormikFormat(error.children);
    }
  }

  return result;
}

export const formikClassValidator = async <T extends object>(
  model: ClassType<T>,
  data: object,
): Promise<ErrorRecordInterface> => {
  try {
    await transformAndValidate(model, data);

    return {};
  } catch (e) {
    return Promise.reject(convertErrorToFormikFormat(e));
  }
};
