import { Book } from '@said-m/bookshelf-common/dist/entities';
import { sortBy as _sortBy } from 'lodash';
import { BooksSortTypes } from '../../../actions';

/** Обёртка на `lodash.sortBy`, чисто для норм тайпингов */
const sortBy = function <Item>(
  collection: Array<Item>,
  // это дичь. `trailing-comma` vs `a rest parameter - no comma`
  // tslint:disable-next-line: trailing-comma
  ...keys: Array<keyof Item>
): Array<Item> {
  return _sortBy(collection, ...keys);
};

export const getSortedBooks = (
  books: Array<Book>,
  sort: BooksSortTypes,
) => {
  switch (sort) {
    case BooksSortTypes.title:
      return sortBy(books, 'title');
    case BooksSortTypes.year:
      return sortBy(books, 'publicationYear');
  }
};
