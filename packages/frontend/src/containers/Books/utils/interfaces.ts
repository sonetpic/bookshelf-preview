import { BooksSortTypes, getBooksIfNeeded, setSort } from '../../../actions';
import { BooksCollectionStateInterface } from '../../../reducers/books/collection';

export interface BooksPropsValuesInterface {
  collection: BooksCollectionStateInterface;
  sort: BooksSortTypes;
}

export interface BooksPropsCallbacksInterface {
  setSort: typeof setSort;
  getBooksIfNeeded: typeof getBooksIfNeeded;
}

export type BooksPropsInterface = BooksPropsValuesInterface & BooksPropsCallbacksInterface;
