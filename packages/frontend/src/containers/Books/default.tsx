import React from 'react';
import { BooksSortTypes } from '../../actions';
import { Book, BookPlaceholder } from '../../components/Book';
import { ControlBox } from '../../components/ControlBox';
import { RadioButtons, RadioButtonsItemInterface, RadioButtonsPropsIsActiveInterface } from '../../components/RadioButtons';
import { getBooksSortTitle } from '../../helpers';
import styles from './Books.module.scss';
import { getSortedBooks } from './utils/helpers';
import { BooksPropsInterface } from './utils/interfaces';

export class Books extends React.Component<BooksPropsInterface> {
  loadData = () => {
    this.props.getBooksIfNeeded();
  }

  componentDidMount() {
    this.loadData();
  }

  componentDidUpdate() {
    if (!this.props.collection.error) {
      this.loadData();
    }
  }

  render() {
    return (
      <main>
        <ControlBox
          main={this.titleEl}
          side={this.sortEl}
        />

        {this.statusMessage}
        <section className={styles.section}>
          {this.bookCollectionEl}

          {this.addBookEl}
        </section>
      </main >
    );
  }

  /** Заголовок раздела */
  get titleEl(): JSX.Element {
    return (
      <h1>
        Список книг
      </h1>
    );
  }

  /** Переключашка сортировки */
  get sortEl(): JSX.Element {
    const isActiveButton: RadioButtonsPropsIsActiveInterface<
      BooksSortTypes
    > = (value) => value === this.props.sort;

    return (
      <div className={styles.sort}>
        <RadioButtons
          buttons={this.sortButtonsDescription}
          isActive={isActiveButton}
        />
      </div>
    );
  }

  /** Не красивое отображение статуса загрузки */
  get statusMessage(): JSX.Element | undefined {
    const books = this.props.collection;

    if (books.error) {
      return (
        <>
          <p>При получения списка книг произошла ошибка</p>
          <button onClick={this.loadData}>
            Повторить попытку
          </button>
        </>
      );
    }

    if (books.isLoading) {
      return (
        <p>Идёт загрузка списка...</p>
      );
    }

    if (!books.data.length) {
      return (
        <p>Пока что книг нет</p>
      );
    }

    return undefined;
  }

  /** Описание кнопок сортировки */
  get sortButtonsDescription(): Array<RadioButtonsItemInterface<
    BooksSortTypes
  >> {
    return [
      {
        title: getBooksSortTitle(BooksSortTypes.title),
        value: BooksSortTypes.title,
        onClick: () => this.props.setSort(BooksSortTypes.title),
      },
      {
        title: getBooksSortTitle(BooksSortTypes.year),
        value: BooksSortTypes.year,
        onClick: () => this.props.setSort(BooksSortTypes.year),
      },
    ];
  }

  /** Список книг (сортируемый) */
  get bookCollectionEl() {
    const books = this.props.collection.data;

    return getSortedBooks(
      books,
      this.props.sort,
    ).map(
      thisBook => {
        const authorNames = thisBook.authors.map(
          thisAuthor => `${thisAuthor.firstName} ${thisAuthor.lastName}`,
        );

        const description = (
          <React.Fragment>
            <p>Год публикации: {thisBook.publicationYear}</p>
            <p>ISBN: {thisBook.isbn}</p>
          </React.Fragment>
        );

        return (
          <Book
            key={thisBook.id}
            subtitle={authorNames.join(', ')}
            description={description}
            title={thisBook.title}
            image={thisBook.image ? thisBook.image : undefined}
          />
        );
      },
    );
  }

  /** Кнопка добавления книги */
  get addBookEl() {
    return !this.props.collection.isLoading &&
      !this.props.collection.error &&
      (
        <BookPlaceholder
          key='add'
          emoji='➕'
          text='Добавить книгу'
        />
      );
  }
}
