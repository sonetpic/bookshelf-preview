import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { BooksSortToggleActionInterface, getBooksIfNeeded, setSort } from '../../actions';
import { RootStateInterface } from '../../reducers';
import { Books } from './default';
import { BooksPropsCallbacksInterface, BooksPropsValuesInterface } from './utils/interfaces';

const mapStateToProps = (
  state: RootStateInterface,
): BooksPropsValuesInterface => (state.books);

const mapDispatchToProps = (
  dispatch: Dispatch<BooksSortToggleActionInterface>,
): BooksPropsCallbacksInterface => bindActionCreators(
  {
    getBooksIfNeeded,
    setSort,
  },
  dispatch,
);

export const BooksStored = connect(
  mapStateToProps,
  mapDispatchToProps,
)(Books);
