import { connect } from 'react-redux';
import { Action, AnyAction, bindActionCreators, Dispatch } from 'redux';
import { addAuthor } from '../../actions';
import { RootStateInterface } from '../../reducers';
import { AddAuthor, AddAuthorPropsCallbacksInterface, AddAuthorPropsValuesInterface } from './default';

const mapStateToProps = (
  state: RootStateInterface,
): AddAuthorPropsValuesInterface => (state);

const mapDispatchToProps = (
  dispatch: Dispatch<Action & AnyAction>,
): AddAuthorPropsCallbacksInterface => bindActionCreators(
  {
    addAuthor,
  },
  dispatch,
);

export const AddAuthorStored = connect(
  mapStateToProps,
  mapDispatchToProps,
)(AddAuthor);
