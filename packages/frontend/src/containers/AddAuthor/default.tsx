import { Length } from 'class-validator';
import React from 'react';
import { addAuthor, AuthorsCollectionAddAsyncActionTypes } from '../../actions';
import { formikClassValidator } from '../../helpers/formik-class-validator';
import { AuthorsStateInterface } from '../../reducers';
import { FormAuthor, FormAuthorPropsInterface } from '../Form';

export class Author {
  @Length(3, 20, {
    message: 'Длина имени должна быть больше 3 и меньше 20 символов',
  })
  firstName!: string;

  @Length(3, 20, {
    message: 'Длина фамилии должна быть больше 3 и меньше 20 символов',
  })
  lastName!: string;
}

export interface AddAuthorPropsValuesInterface {
  authors: AuthorsStateInterface;
}

export interface AddAuthorPropsCallbacksInterface {
  addAuthor: typeof addAuthor;
}

export type AddAuthorPropsInterface = AddAuthorPropsValuesInterface &
  AddAuthorPropsCallbacksInterface;

export class AddAuthor extends React.Component<
  AddAuthorPropsInterface
> {
  render() {
    const submit: FormAuthorPropsInterface['submit'] = {
      title: 'Добавить',
      action: async (values, {
        setSubmitting,
        resetForm,
        setStatus,
        setError,
      }) => {
        const result = await this.props.addAuthor(values);

        // FIXME: исправить interface return-а у result-ов
        switch (result.type) {
          // @ts-ignore
          case AuthorsCollectionAddAsyncActionTypes.success:
            resetForm();
            break;
          // @ts-ignore
          case AuthorsCollectionAddAsyncActionTypes.failure:
            // @ts-ignore
            setError(result.error);
            break;
        }

        setSubmitting(false);
      },
    };

    const validate: FormAuthorPropsInterface['validate'] = async (values) =>
      formikClassValidator(Author, values);

    return (
      <FormAuthor
        title='Добавить автора'
        submit={submit}
        validate={validate}
      />
    );
  }
}
