import React from 'react';
import { NavLink } from 'react-router-dom';
import { Navigation, NavigationPropsItemInterface } from '../../components/Navigation';
import { Wrap } from '../../components/Wrap';
import { ThemeSwitcherStored } from '../ThemeSwitcher';
import styles from './Header.module.scss';

export interface HeaderPropsTitleInterface {
  to: string;
  emoji: string;
  text: string;
}

interface HeaderPropsInterface {
  title: HeaderPropsTitleInterface;
  items: Array<NavigationPropsItemInterface>;
}

export const Header: React.FC<
  HeaderPropsInterface
> = ({
  title,
  items,
}) => {
  return (
    <header className={styles.wrap}>
      <Wrap classNames={[styles.content]}>
        <h1 className={styles.title}>
          <NavLink exact={true} to={title.to}>
            <span
              className={styles.emoji}
              role='img'
              aria-label='page emoji symbol'
            >
              {title.emoji}
            </span>
            {title.text}
          </NavLink>
        </h1>

        <Navigation
          items={items}
          className={styles.nav}
        />

        <ThemeSwitcherStored/>
      </Wrap>
    </header>
  );
};
