import { connect } from 'react-redux';
import { Action, AnyAction, bindActionCreators, Dispatch } from 'redux';
import { Authors } from '.';
import { getAuthorsIfNeeded } from '../../actions';
import { RootStateInterface } from '../../reducers';
import { AuthorsPropsCallbacksInterface, AuthorsPropsValuesInterface } from './utils/interfaces';

const mapStateToProps = (
  state: RootStateInterface,
): AuthorsPropsValuesInterface => (state);

const mapDispatchToProps = (
  dispatch: Dispatch<Action & AnyAction>,
): AuthorsPropsCallbacksInterface => bindActionCreators(
  {
    getAuthorsIfNeeded,
  },
  dispatch,
);

export const AuthorsStored = connect(
  mapStateToProps,
  mapDispatchToProps,
)(Authors);
