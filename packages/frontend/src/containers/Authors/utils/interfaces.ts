import { getAuthorsIfNeeded } from '../../../actions';
import { AuthorsCollectionStateInterface } from '../../../reducers';

export interface AuthorsPropsValuesInterface {
  authors: AuthorsCollectionStateInterface;
}

export interface AuthorsPropsCallbacksInterface {
  getAuthorsIfNeeded: typeof getAuthorsIfNeeded;
}

export type AuthorsPropsInterface =
  AuthorsPropsValuesInterface &
  AuthorsPropsCallbacksInterface;
