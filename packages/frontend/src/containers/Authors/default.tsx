import React from 'react';
import { Author, AuthorPropsInterface } from '../../components/Author';
import { newAuthorPath, toEditAuthorPath, toViewAuthorPath } from '../../layouts/App/routes';
import styles from './Authors.module.scss';
import { AuthorsPropsInterface } from './utils/interfaces';

export class Authors extends React.Component<AuthorsPropsInterface> {
  loadData = () => {
    this.props.getAuthorsIfNeeded();
  }

  componentDidMount() {
    this.loadData();
  }

  componentDidUpdate() {
    if (!this.props.authors.error) {
      this.loadData();
    }
  }

  render() {
    return (
      <main>
        <h1>
          Список авторов
        </h1>

        {this.statusMessage}

        <section className={styles.section}>
          {this.authorCollectionEl}

          {this.addAuthorEl}
        </section>
      </main >
    );
  }

  /** Список авторов */
  get authorCollectionEl() {
    return this.props.authors.data.map(
      thisAuthor => {
        const { id, firstName, lastName } = thisAuthor;

        const pathParams = { authorId: id };

        const actionPaths: AuthorPropsInterface['to'] = {
          view: toViewAuthorPath(pathParams),
          edit: toEditAuthorPath(pathParams),
        };

        return (
          <Author
            key={id}
            to={actionPaths}
            title={`${firstName} ${lastName}`}
          />
        );
      },
    );
  }

  /** Кнопка добавления автора */
  get addAuthorEl() {
    return !this.props.authors.isLoading &&
      !this.props.authors.error &&
      (
        <Author
          key='add'
          to={({ main: newAuthorPath })}
          title={`➕ Добавить автора`}
        />
      );
  }

  /** Не красивое отображение статуса загрузки */
  get statusMessage(): JSX.Element | undefined {
    const authors = this.props.authors;

    if (authors.error) {
      return (
        <>
          <p>При получения списка авторов произошла ошибка</p>
          <button onClick={this.loadData}>
            Повторить попытку
          </button>
        </>
      );
    }

    if (authors.isLoading) {
      return (
        <p>Идёт загрузка списка...</p>
      );
    }

    if (!authors.data.length) {
      return (
        <p>Пока что нет ни одного автора</p>
      );
    }

    return undefined;
  }
}
