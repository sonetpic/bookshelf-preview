import { Formik, FormikConfig } from 'formik';
import React from 'react';
import { formCreator } from '../../components/FormCreator';

interface FormAuthorValuesInterface {
  firstName: string;
  lastName: string;
}

interface FormAuthorSubmitInterface {
  title: string;
  action: FormikPropsInterface['onSubmit'];
}

export interface FormAuthorPropsInterface {
  title: string;
  initial?: FormAuthorValuesInterface;
  submit: FormAuthorSubmitInterface;
  validate: FormikPropsInterface['validate'];
  isViewMode?: boolean;
}

type FormikPropsInterface = FormikConfig<FormAuthorValuesInterface>;

const FORM_AUTHOR_INITIAL = {
  firstName: '',
  lastName: '',
} as const;

export const FormAuthor: React.FC<
  FormAuthorPropsInterface
> = (
  {
    title,
    initial = FORM_AUTHOR_INITIAL,
    submit,
    validate,
    isViewMode,
  }: FormAuthorPropsInterface,
) => {
  const form = formCreator({
    fields: [
      {
        name: 'firstName',
        title: 'Имя',
      },
      {
        name: 'lastName',
        title: 'Фамилия',
      },
    ],
    submit: submit.title,
    isViewMode,
  });

  return(
    <>
      <h1>{title}</h1>

      <Formik
        enableReinitialize={true}
        isInitialValid={true}
        initialValues={initial}
        render={form}
        onSubmit={submit.action}
        validate={validate}
      />
    </>
  );
};
