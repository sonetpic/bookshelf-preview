import { Length } from 'class-validator';
import React from 'react';
import { Redirect } from 'react-router-dom';
import { AuthorsCollectionGetAsyncActionTypes, AuthorsCollectionUpdateAsyncActionTypes, getAuthors, updateAuthor } from '../../actions';
import { formikClassValidator } from '../../helpers/formik-class-validator';
import { authorsPath } from '../../layouts/App/routes';
import { FormAuthor, FormAuthorPropsInterface } from '../Form';

export class Author {
  @Length(3, 20, {
    message: 'Длина имени должна быть больше 3 и меньше 20 символов',
  })
  firstName!: string;

  @Length(3, 20, {
    message: 'Длина фамилии должна быть больше 3 и меньше 20 символов',
  })
  lastName!: string;
}

export interface UpdateAuthorPropsCallbacksInterface {
  getAuthor: typeof getAuthors;
  updateAuthor: typeof updateAuthor;
}

export type UpdateAuthorPropsInterface = UpdateAuthorPropsCallbacksInterface & {
  authorId?: number;
  isViewMode?: boolean;
};

interface UpdateAuthorStateInterface {
  author: Author | undefined;
  isLoadingError: boolean;
  isLoading: boolean;
}

export class UpdateAuthor extends React.Component<
  UpdateAuthorPropsInterface,
  UpdateAuthorStateInterface
> {
  state: Readonly<UpdateAuthorStateInterface> = {
    author: undefined,
    isLoadingError: false,
    isLoading: true,
  };

  async componentDidMount() {
    this.loadAuthor().catch(() => {
      this.setState({
        isLoadingError: true,
        isLoading: false,
      });
    });
  }

  async loadAuthor() {
    this.setState({
      isLoading: true,
    });

    const loaded = await this.props.getAuthor(this.props.authorId);

    switch (loaded.type) {
      case AuthorsCollectionGetAsyncActionTypes.failure:
        this.setState({
          isLoadingError: true,
        });
        break;
      case AuthorsCollectionGetAsyncActionTypes.success:
        this.setState({
          // FIXME:
          // @ts-ignore
          author: (loaded.response as Array<Author>)[0],
          isLoadingError: false,
        });
    }

    this.setState({
      isLoading: false,
    });
  }

  render() {
    const submit: FormAuthorPropsInterface['submit'] = {
      title: 'Обновить',
      action: async (values, {
        setSubmitting,
        resetForm,
        setError,
      }) => {
        const result = await this.props.updateAuthor({
          ...values,
        });

        switch (result.type) {
          case AuthorsCollectionUpdateAsyncActionTypes.success:
            this.setState({
              // FIXME:
              // @ts-ignore
              author: (result.response as Author),
            });
            break;
          case AuthorsCollectionUpdateAsyncActionTypes.failure:
            // @ts-ignore
            setError(result.error);
            break;
        }

        setSubmitting(false);
      },
    };

    const validate: FormAuthorPropsInterface['validate'] = async (values) =>
      formikClassValidator(Author, values);

    if (!this.state.author && !this.state.isLoading) {
      return <Redirect to={authorsPath}/>;
    }

    const render = this.state.isLoading
      ? 'Идёт загрузка'
      : (
        <FormAuthor
          title={(this.props.isViewMode ? 'Информация' : 'Изменить информацию') + ' об авторе'}
          submit={submit}
          validate={validate}
          initial={this.state.author}
          isViewMode={this.props.isViewMode}
        />
      );

    return render;
  }
}
