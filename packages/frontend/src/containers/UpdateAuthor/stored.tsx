import { connect } from 'react-redux';
import { Action, AnyAction, bindActionCreators, Dispatch } from 'redux';
import { getAuthors, updateAuthor } from '../../actions';
import { UpdateAuthor, UpdateAuthorPropsCallbacksInterface } from './default';

const mapDispatchToProps = (
  dispatch: Dispatch<Action & AnyAction>,
): UpdateAuthorPropsCallbacksInterface => bindActionCreators(
  {
    getAuthor: getAuthors,
    updateAuthor,
  },
  dispatch,
);

export const UpdateAuthorStored = connect(
  null,
  mapDispatchToProps,
)(UpdateAuthor);
