import { toggleTheme } from '../../../actions';
import { PrefersColorSchemeEnum } from '../../../helpers/detect-prefers-theme';

export interface ThemeSwitcherPropsValuesInterface {
  theme: PrefersColorSchemeEnum;
}

export interface ThemeSwitcherPropsCallbacksInterface {
  toggleTheme: typeof toggleTheme;
}

export type ThemeSwitcherPropsInterface =
  ThemeSwitcherPropsValuesInterface &
  ThemeSwitcherPropsCallbacksInterface;
