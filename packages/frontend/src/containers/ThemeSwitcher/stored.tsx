import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { SettingsThemeToggleActionInterface, toggleTheme } from '../../actions';
import { RootStateInterface } from '../../reducers';
import { ThemeSwitcher } from './default';
import { ThemeSwitcherPropsCallbacksInterface, ThemeSwitcherPropsValuesInterface } from './utils/interfaces';

const mapStateToProps = (
  state: RootStateInterface,
): ThemeSwitcherPropsValuesInterface => (state.settings);

const mapDispatchToProps = (
  dispatch: Dispatch<SettingsThemeToggleActionInterface>,
): ThemeSwitcherPropsCallbacksInterface => bindActionCreators(
  {
    toggleTheme,
  },
  dispatch,
);

export const ThemeSwitcherStored = connect(
  mapStateToProps,
  mapDispatchToProps,
)(ThemeSwitcher);
