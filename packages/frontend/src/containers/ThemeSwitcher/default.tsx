import React from 'react';
import { ThemeButton } from '../../components/ThemeButton';
import useTheme from '../../hooks/use-theme';
import { ThemeSwitcherPropsInterface } from './utils/interfaces';

export const ThemeSwitcher: React.FC<
  ThemeSwitcherPropsInterface
> = ({
  theme,
  toggleTheme,
}) => {
  useTheme(theme);

  return (
    <ThemeButton
      theme={theme}
      onClick={toggleTheme}
    />
  );
};
