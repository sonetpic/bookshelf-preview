import { Author, Book } from '@said-m/bookshelf-common/dist/entities';
import { schema } from 'normalizr';

export const booksSchema = new schema.Entity<Book>('books');
export const authorsSchema = new schema.Entity<Author>('authors');
