import { AxiosError } from 'axios';
import hasProperty from 'ts-has-property';

export const isAxiosError = (
  // tslint:disable-next-line: no-any
  data: any,
): data is AxiosError => hasProperty(
  {
    ...data,
  },
  'isAxiosError',
  'boolean',
);
