import { ApiErrorInterface } from '@said-m/bookshelf-common/dist/interfaces/api';
import { Action } from 'redux';

export type ApiErrorActionInterface = Omit<
  ApiErrorInterface,
  'status'
>;

export type ApiRequestActionInterface<Type> =
  Action<Type>;

export type ApiSuccessActionInterface<
  Type,
  R = unknown
> = Action<Type> & {
  response: R;
  receivedAt: number;
  isPartial?: boolean;
};

export type ApiFailureActionInterface<
  Type
> = Action<Type> & {
  error: ApiErrorActionInterface;
};

export interface ApiMiddlewareActionTypesInterface {
  request: string;
  success: string;
  failure: string;
}

export type ApiActionsInterface<
  R extends ApiRequestActionInterface<unknown>,
  S extends ApiSuccessActionInterface<unknown>,
  F extends ApiFailureActionInterface<unknown>,
> = R | S | F;
