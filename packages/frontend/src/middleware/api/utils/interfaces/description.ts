import { ApiSettingsInterface } from '@said-m/bookshelf-common/dist/interfaces/api';
import { ObjectInterface } from '@said-m/common/dist/interfaces';
import { schema } from 'normalizr';
import { Action } from 'redux';
import { ApiMiddlewareActionTypesInterface } from '.';
import { API } from '../..';

type EndpointFunctionInterface = (stage: unknown) => string;

export type ApiPayloadInterface =
  // tslint:disable-next-line: no-any
  Array<any> | ObjectInterface<any>;

type ApiMiddlewareDataInterface<
  Payload extends
    ApiPayloadInterface |
    void = void,
> = {
  endpoint: string | EndpointFunctionInterface;
  schema: schema.Entity;
  types: ApiMiddlewareActionTypesInterface;
  settings?: ApiSettingsInterface;
  /** Флаг частичного обновления - не нужно фиксировать время обновления */
  isPartial?: boolean;
} & (
  Payload extends void
    ? {}
    : {
      payload: Payload;
    }
);

export interface ApiActionInterface<
  Payload extends ApiPayloadInterface | void = void,
  // tslint:disable-next-line: no-any
  Type = any
> extends Action<Type> {
  [API]: ApiMiddlewareDataInterface<Payload>;
}
