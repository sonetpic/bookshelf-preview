import { ApiRequest } from '@said-m/bookshelf-common/dist/dto';
import { ApiError, isApiError } from '@said-m/bookshelf-common/dist/helpers';
import { ApiDataInterface, ApiSettingsInterface } from '@said-m/bookshelf-common/dist/interfaces/api';
import Axios from 'axios';
import { schema } from 'normalizr';
import hasProperty from 'ts-has-property';
import { isAxiosError } from './utils/helpers';
import { ApiPayloadInterface } from './utils/interfaces';

interface MakeRequestInputInterface {
  url: string;
  schema: schema.Entity;
  payload?: ApiPayloadInterface;
  settings?: ApiSettingsInterface;
}

export const makeRequest = (
  input: MakeRequestInputInterface,
) => {
  const body: ApiRequest<ApiDataInterface> = {
    data: input.payload || {},
    settings: input.settings,
  };

  return Axios.post<ApiRequest<ApiDataInterface>>(
    input.url,
    body,
    {
      responseType: 'json',
    },
  )
    .then(response => {
      if (
        response.status < 200 ||
        response.status >= 300
      ) {
        throw new Error('Ошибка при выполнении запроса');
      }
      if (
        !hasProperty(response.data, 'data', 'object') &&
        !hasProperty(response.data, 'data', 'array')
      ) {
        throw new Error('Не удалось обработать ответ');
      }

      return response.data.data;
      // return {
      //   ...normalize(response.data.data, input.schema),
      // };
    })
    .catch(error => {
      if (isAxiosError(error)) {
        if (error.response) {
          if (
            hasProperty(error.response.data, 'data', 'object') &&
            isApiError(error.response.data.data)
          ) {
            throw new ApiError(error.response.data.data);
          }

          throw new ApiError({
            status: error.response.status,
            message: error.message,
            description: error.response.statusText,
          });
        }
      }

      if (error instanceof Error) {
        throw new ApiError({
          status: 400,
          message: error.message,
        });
      }

      throw new ApiError({
        status: 400,
        message: 'Произошла неожиданная ошибка',
      });
    });
};
