import { ApiError } from '@said-m/bookshelf-common/dist/helpers';
import { isFunction } from 'lodash';
import { Action, Dispatch, Middleware } from 'redux';
import { BooksActionsInterface } from '../../actions';
import { makeRequest } from './make-request';
import { ApiActionInterface, ApiActionsInterface, ApiFailureActionInterface, ApiPayloadInterface, ApiRequestActionInterface, ApiSuccessActionInterface } from './utils/interfaces';

export * from './utils/interfaces';

export const API: unique symbol = Symbol('Bookshelf API');

type MiddlewareActionInterface = Action &
    ApiActionsInterface<
      ApiRequestActionInterface<unknown>,
      ApiSuccessActionInterface<unknown>,
      ApiFailureActionInterface<unknown>
    >
   &
  Partial<
    ApiActionInterface<
      ApiPayloadInterface
    >
  >;

type BookshelfApiActions = BooksActionsInterface;

export const apiMiddleware: Middleware<
  ApiActionInterface,
  unknown,
  Dispatch<BookshelfApiActions>
> = store => next => (
  action: MiddlewareActionInterface,
) => {
  const apiAction = action[API];

  if (apiAction === undefined) {
    return next(action);
  }

  const url = isFunction(apiAction.endpoint)
    ? apiAction.endpoint(store.getState())
    : apiAction.endpoint;

  const actionWith = (data: MiddlewareActionInterface) => {
    const finalAction: MiddlewareActionInterface = {
      ...action,
      ...data,
    };

    delete finalAction[API];

    return finalAction;
  };

  next(actionWith({
    type: apiAction.types.request,
  }));

  return makeRequest({
    url,
    payload: apiAction.payload,
    settings: apiAction.settings,
    schema: apiAction.schema,
  })
    .then(
      response => {
        return next(
          actionWith({
            type: apiAction.types.success,
            response,
            isPartial: apiAction.isPartial,
            receivedAt: Date.now(),
          }),
        );
      },
      (error: ApiError) => {
        return next(
          actionWith({
            type: apiAction.types.failure,
            error: {
              message: 'Ошибка запроса: ' + error.status.toString(),
              description: error.message + (
                error.description
                  ? `:\n${error.description}`
                  : ''
              ),
            },
          }),
        );
      },
    );
};
