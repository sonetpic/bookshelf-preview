import React from 'react';
import { Redirect, Route, RouteProps, Switch } from 'react-router-dom';
import { NavigationPropsItemInterface } from '../../components/Navigation';
import { Wrap } from '../../components/Wrap';
import { AddAuthorStored } from '../../containers/AddAuthor';
import { AuthorsStored } from '../../containers/Authors';
import { BooksStored } from '../../containers/Books';
import { Header, HeaderPropsTitleInterface } from '../../containers/Header';
import { UpdateAuthorStored } from '../../containers/UpdateAuthor';
import './App.scss';
import { APP_ROOT_PATH, authorsPath, booksPath, editAuthorPath, newAuthorPath, viewAuthorPath } from './routes';

export const App: React.FC = () => {
  const title: HeaderPropsTitleInterface = {
    to: APP_ROOT_PATH,
    emoji: '📚',
    text: 'Bookshelf',
  };

  const navigationItems: Array<
    NavigationPropsItemInterface
  > = [
      {
        to: booksPath,
        title: '📗 Книги',
      },
      {
        to: authorsPath,
        title: '👥 Авторы',
      },
    ];

  const UpdateAuthorRenderHandler: RouteProps['render'] = props => (
    <UpdateAuthorStored authorId={parseInt(props.match.params.authorId)} />
  );

  const ViewAuthorRenderHandler: RouteProps['render'] = props => (
    <UpdateAuthorStored
      authorId={parseInt(props.match.params.authorId)}
      isViewMode={true}
    />
  );

  return (
    <>
      <Header
        title={title}
        items={navigationItems}
      />

      <Wrap>
        <Switch>
          <Route exact={true} path={APP_ROOT_PATH}>
            <Redirect to={booksPath} />
          </Route>
          <Route exact={true} path={booksPath}>
            <BooksStored />
          </Route>
          <Route exact={true} path={authorsPath}>
            <AuthorsStored />
          </Route>
          <Route exact={true} path={newAuthorPath}>
            <AddAuthorStored />
          </Route>
          <Route
            exact={true}
            path={viewAuthorPath}
            render={ViewAuthorRenderHandler}
          />
          <Route
            exact={true}
            path={editAuthorPath}
            render={UpdateAuthorRenderHandler}
          />
          <Redirect to={APP_ROOT_PATH} />
        </Switch>
      </Wrap>
    </>
  );
};
