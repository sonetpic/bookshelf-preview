import { compile } from 'path-to-regexp';

// ROOT
export const APP_ROOT_PATH = '/bookshelf';

// BOOKS
export const booksPath = `${APP_ROOT_PATH}/books`;

// AUTHORS
type AuthorIdPathInterface = Record<'authorId', number>;

export const authorsPath = `${APP_ROOT_PATH}/authors`;

export const newAuthorPath = `${authorsPath}/new`;

export const viewAuthorPath = `${authorsPath}/:authorId`;
export const toViewAuthorPath = compile<AuthorIdPathInterface>(viewAuthorPath);

export const editAuthorPath = `${viewAuthorPath}/edit`;
export const toEditAuthorPath = compile<AuthorIdPathInterface>(editAuthorPath);
