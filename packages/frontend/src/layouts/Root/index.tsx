import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import { store } from '../../store';
import { App } from '../App';
import { APP_ROOT_PATH } from '../App/routes';

const ROOT_PATHS = {
  main: '/',
  bookshelf: APP_ROOT_PATH,
};

export const Root: React.FC = () => {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Switch>
          <Route
            exact={true}
            path={ROOT_PATHS.main}
          >
            <Redirect to={ROOT_PATHS.bookshelf} />
          </Route>
          <Route path={ROOT_PATHS.bookshelf}>
            <App />
          </Route>
          <Redirect to={ROOT_PATHS.main} />
        </Switch>
      </BrowserRouter>
    </Provider>
  );
};
