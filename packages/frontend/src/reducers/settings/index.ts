import { SettingsActionsInterface, SettingsThemeActionTypes } from '../../actions';
import { detectPrefersTheme, getSessionTheme, PrefersColorSchemeEnum, sessionTheme } from '../../helpers/detect-prefers-theme';

export interface SettingsStateInterface {
  theme: PrefersColorSchemeEnum;
}

const initialState: SettingsStateInterface = {
  theme: getSessionTheme() || detectPrefersTheme(),
};

export const settingsReducer = (
  state = initialState,
  action: SettingsActionsInterface,
): SettingsStateInterface => {
  switch (action.type) {
    case SettingsThemeActionTypes.set:
      return {
        ...state,
        theme: setTheme(action.theme),
      };
    case SettingsThemeActionTypes.toggle:
      return {
        ...state,
        theme: toggleTheme(state.theme),
      };
    case SettingsThemeActionTypes.reset:
      return {
        ...state,
        theme: detectPrefersTheme(),
      };
    default:
      return state;
  }
};

function setTheme(
  theme: PrefersColorSchemeEnum,
) {
  sessionStorage.setItem(sessionTheme, theme);

  return theme;
}

function toggleTheme(
  theme: PrefersColorSchemeEnum,
) {
  switch (theme) {
    case PrefersColorSchemeEnum.light:
    case PrefersColorSchemeEnum.no:
      return setTheme(PrefersColorSchemeEnum.dark);
    case PrefersColorSchemeEnum.dark:
      return setTheme(PrefersColorSchemeEnum.light);
  }
}
