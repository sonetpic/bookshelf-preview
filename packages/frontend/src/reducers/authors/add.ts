import { authorsCollectionInitialState, AuthorsCollectionStateInterface } from '.';
import { AuthorsCollectionAddActionsInterface, AuthorsCollectionAddAsyncActionTypes } from '../../actions';

export const authorsCollectionAddReducer = (
  state = authorsCollectionInitialState,
  action: AuthorsCollectionAddActionsInterface,
): AuthorsCollectionStateInterface => {
  switch (action.type) {
    case AuthorsCollectionAddAsyncActionTypes.request:
      return {
        ...state,
        isLoading: true,
      };
    case AuthorsCollectionAddAsyncActionTypes.success:
      return {
        ...state,
        isLoading: false,
        error: false,
        data: [
          ...state.data,
          action.response,
        ],
      };
    case AuthorsCollectionAddAsyncActionTypes.failure:
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };
    default:
      return state;
  }
};
