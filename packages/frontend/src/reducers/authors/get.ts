import { unionBy } from 'lodash';
import { authorsCollectionInitialState, AuthorsCollectionStateInterface } from '.';
import { AuthorsCollectionGetActionsInterface, AuthorsCollectionGetAsyncActionTypes } from '../../actions';

export const authorsCollectionGetReducer = (
  state = authorsCollectionInitialState,
  action: AuthorsCollectionGetActionsInterface,
): AuthorsCollectionStateInterface => {
  switch (action.type) {
    case AuthorsCollectionGetAsyncActionTypes.request:
      return {
        ...state,
        error: false,
        isLoading: true,
      };
    case AuthorsCollectionGetAsyncActionTypes.success:
      return {
        ...state,
        error: false,
        isLoading: false,
        ...(
          action.isPartial
            ? {
              data: unionBy(state.data, action.response, 'id'),
              receivedAt: state.receivedAt,
            }
            : {
              data: action.response,
              receivedAt: action.receivedAt,
            }
        ),
      };
    case AuthorsCollectionGetAsyncActionTypes.failure:
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };
    default:
      return state;
  }
};
