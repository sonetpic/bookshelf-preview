import { authorsCollectionInitialState, AuthorsCollectionStateInterface } from '.';
import { AuthorsCollectionUpdateActionsInterface, AuthorsCollectionUpdateAsyncActionTypes } from '../../actions';

export const authorsCollectionUpdateReducer = (
  state = authorsCollectionInitialState,
  action: AuthorsCollectionUpdateActionsInterface,
): AuthorsCollectionStateInterface => {
  switch (action.type) {
    case AuthorsCollectionUpdateAsyncActionTypes.request:
      return {
        ...state,
        isLoading: true,
      };
    case AuthorsCollectionUpdateAsyncActionTypes.success:
      return {
        ...state,
        error: false,
        isLoading: false,
        data: state.data.map(
          thisAuthor => thisAuthor.id !== action.response.id
            ? thisAuthor
            : {
              ...thisAuthor,
              ...action.response,
            },
        ),
      };
    case AuthorsCollectionUpdateAsyncActionTypes.failure:
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };
    default:
      return state;
  }
};
