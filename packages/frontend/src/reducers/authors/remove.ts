import { authorsCollectionInitialState, AuthorsCollectionStateInterface } from '.';
import { AuthorsCollectionRemoveActionsInterface, AuthorsCollectionRemoveAsyncActionTypes } from '../../actions';

export const authorsCollectionRemoveReducer = (
  state = authorsCollectionInitialState,
  action: AuthorsCollectionRemoveActionsInterface,
): AuthorsCollectionStateInterface => {
  switch (action.type) {
    case AuthorsCollectionRemoveAsyncActionTypes.request:
      return {
        ...state,
        isLoading: true,
      };
    case AuthorsCollectionRemoveAsyncActionTypes.success:
      return {
        ...state,
        error: false,
        isLoading: false,
        data: state.data.filter(
          thisAuthor => !(action.response.includes(thisAuthor.id)),
        ),
      };
    case AuthorsCollectionRemoveAsyncActionTypes.failure:
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };
    default:
      return state;
  }
};
