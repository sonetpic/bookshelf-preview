import { AuthorDto } from '@said-m/bookshelf-common/dist/entities';
import { AuthorsCollectionActionsInterface, AuthorsCollectionAddAsyncActionTypes, AuthorsCollectionGetAsyncActionTypes, AuthorsCollectionRemoveAsyncActionTypes, AuthorsCollectionUpdateAsyncActionTypes } from '../../actions';
import { AsyncActionStateInterface } from '../utils/interfaces';
import { authorsCollectionAddReducer } from './add';
import { authorsCollectionGetReducer } from './get';
import { authorsCollectionRemoveReducer } from './remove';
import { authorsCollectionUpdateReducer } from './update';

export type AuthorsCollectionStateInterface =
  AsyncActionStateInterface<
    Array<AuthorDto>
  >;

export const authorsCollectionInitialState:
AuthorsCollectionStateInterface = {
  data: [],
  isLoading: false,
  error: false,
  receivedAt: 0,
};

export const authorsCollectionReducer = (
  state = authorsCollectionInitialState,
  action: AuthorsCollectionActionsInterface,
): AuthorsCollectionStateInterface => {
  switch (action.type) {
    case AuthorsCollectionGetAsyncActionTypes.request:
    case AuthorsCollectionGetAsyncActionTypes.success:
    case AuthorsCollectionGetAsyncActionTypes.failure:
      return authorsCollectionGetReducer(state, action);
    case AuthorsCollectionAddAsyncActionTypes.request:
    case AuthorsCollectionAddAsyncActionTypes.success:
    case AuthorsCollectionAddAsyncActionTypes.failure:
      return authorsCollectionAddReducer(state, action);
    case AuthorsCollectionUpdateAsyncActionTypes.request:
    case AuthorsCollectionUpdateAsyncActionTypes.success:
    case AuthorsCollectionUpdateAsyncActionTypes.failure:
      return authorsCollectionUpdateReducer(state, action);
    case AuthorsCollectionRemoveAsyncActionTypes.request:
    case AuthorsCollectionRemoveAsyncActionTypes.success:
    case AuthorsCollectionRemoveAsyncActionTypes.failure:
      return authorsCollectionRemoveReducer(state, action);
    default:
      return state;
  }
};

export type AuthorsStateInterface = AuthorsCollectionStateInterface;

export const authorsReducer = authorsCollectionReducer;
