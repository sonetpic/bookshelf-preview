import { combineReducers } from 'redux';
import { booksCollectionReducer, BooksCollectionStateInterface } from './collection';
import { booksSortReducer, BooksSortStateInterface } from './sort';

export interface BooksStateInterface {
  collection: BooksCollectionStateInterface;
  sort: BooksSortStateInterface;
}

export const booksReducer = combineReducers<
  BooksStateInterface
> ({
  collection: booksCollectionReducer,
  sort: booksSortReducer,
});
