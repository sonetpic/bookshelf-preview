import { BooksSortActionsInterface, BooksSortActionTypes, BooksSortTypes } from '../../actions';

export type BooksSortStateInterface = BooksSortTypes;

const initialState = BooksSortTypes.title;

export const booksSortReducer = (
  state = initialState,
  action: BooksSortActionsInterface,
): BooksSortStateInterface => {
  switch (action.type) {
    case BooksSortActionTypes.set:
      return action.sort;
    case BooksSortActionTypes.toggle:
      return toggleSort(state);
    default:
      return state;
  }
};

function toggleSort(
  sort: BooksSortTypes,
): BooksSortTypes {
  switch (sort) {
    case BooksSortTypes.title:
      return BooksSortTypes.year;
    case BooksSortTypes.year:
      return BooksSortTypes.title;
  }
}
