import { booksCollectionInitialState, BooksCollectionStateInterface } from '.';
import { BooksCollectionUpdateActionsInterface, BooksCollectionUpdateAsyncActionTypes } from '../../../actions';

export const booksCollectionUpdateReducer = (
  state = booksCollectionInitialState,
  action: BooksCollectionUpdateActionsInterface,
): BooksCollectionStateInterface => {
  switch (action.type) {
    case BooksCollectionUpdateAsyncActionTypes.request:
      return {
        ...state,
        isLoading: true,
      };
    case BooksCollectionUpdateAsyncActionTypes.success:
      return {
        ...state,
        error: false,
        isLoading: false,
        data: state.data.map(
          thisBook => thisBook.id !== action.response.id
            ? thisBook
            : {
              ...thisBook,
              ...action.response,
            },
        ),
      };
    case BooksCollectionUpdateAsyncActionTypes.failure:
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };
    default:
      return state;
  }
};
