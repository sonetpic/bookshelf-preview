import { Book } from '@said-m/bookshelf-common/dist/entities';
import { BooksCollectionActionsInterface, BooksCollectionAddAsyncActionTypes, BooksCollectionGetAsyncActionTypes, BooksCollectionRemoveAsyncActionTypes, BooksCollectionUpdateAsyncActionTypes } from '../../../actions';
import { AsyncActionStateInterface } from '../../utils/interfaces';
import { booksCollectionAddReducer } from './add';
import { booksCollectionGetReducer } from './get';
import { booksCollectionRemoveReducer } from './remove';
import { booksCollectionUpdateReducer } from './update';

export type BooksCollectionStateInterface =
  AsyncActionStateInterface<
    Array<Book>
  >;

export const booksCollectionInitialState:
BooksCollectionStateInterface = {
  data: [],
  isLoading: false,
  error: false,
  receivedAt: 0,
};

export const booksCollectionReducer = (
  state = booksCollectionInitialState,
  action: BooksCollectionActionsInterface,
): BooksCollectionStateInterface => {
  switch (action.type) {
    case BooksCollectionGetAsyncActionTypes.request:
    case BooksCollectionGetAsyncActionTypes.success:
    case BooksCollectionGetAsyncActionTypes.failure:
      return booksCollectionGetReducer(state, action);
    case BooksCollectionAddAsyncActionTypes.request:
    case BooksCollectionAddAsyncActionTypes.success:
    case BooksCollectionAddAsyncActionTypes.failure:
      return booksCollectionAddReducer(state, action);
    case BooksCollectionUpdateAsyncActionTypes.request:
    case BooksCollectionUpdateAsyncActionTypes.success:
    case BooksCollectionUpdateAsyncActionTypes.failure:
      return booksCollectionUpdateReducer(state, action);
    case BooksCollectionRemoveAsyncActionTypes.request:
    case BooksCollectionRemoveAsyncActionTypes.success:
    case BooksCollectionRemoveAsyncActionTypes.failure:
      return booksCollectionRemoveReducer(state, action);
    default:
      return state;
  }
};
