import { booksCollectionInitialState, BooksCollectionStateInterface } from '.';
import { BooksCollectionRemoveActionsInterface, BooksCollectionRemoveAsyncActionTypes } from '../../../actions';

export const booksCollectionRemoveReducer = (
  state = booksCollectionInitialState,
  action: BooksCollectionRemoveActionsInterface,
): BooksCollectionStateInterface => {
  switch (action.type) {
    case BooksCollectionRemoveAsyncActionTypes.request:
      return {
        ...state,
        isLoading: true,
      };
    case BooksCollectionRemoveAsyncActionTypes.success:
      return {
        ...state,
        error: false,
        isLoading: false,
        data: state.data.filter(
          thisBook => !(action.response.includes(thisBook.id)),
        ),
      };
    case BooksCollectionRemoveAsyncActionTypes.failure:
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };
    default:
      return state;
  }
};
