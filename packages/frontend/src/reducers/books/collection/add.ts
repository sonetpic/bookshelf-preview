import { booksCollectionInitialState, BooksCollectionStateInterface } from '.';
import { BooksCollectionAddActionsInterface, BooksCollectionAddAsyncActionTypes } from '../../../actions';

export const booksCollectionAddReducer = (
  state = booksCollectionInitialState,
  action: BooksCollectionAddActionsInterface,
): BooksCollectionStateInterface => {
  switch (action.type) {
    case BooksCollectionAddAsyncActionTypes.request:
      return {
        ...state,
        isLoading: true,
      };
    case BooksCollectionAddAsyncActionTypes.success:
      return {
        ...state,
        isLoading: false,
        error: false,
        data: [
          ...state.data,
          action.response,
        ],
      };
    case BooksCollectionAddAsyncActionTypes.failure:
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };
    default:
      return state;
  }
};
