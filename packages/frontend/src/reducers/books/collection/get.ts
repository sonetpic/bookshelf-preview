import { booksCollectionInitialState, BooksCollectionStateInterface } from '.';
import { BooksCollectionGetActionsInterface, BooksCollectionGetAsyncActionTypes } from '../../../actions';

export const booksCollectionGetReducer = (
  state = booksCollectionInitialState,
  action: BooksCollectionGetActionsInterface,
): BooksCollectionStateInterface => {
  switch (action.type) {
    case BooksCollectionGetAsyncActionTypes.request:
      return {
        ...state,
        error: false,
        isLoading: true,
      };
    case BooksCollectionGetAsyncActionTypes.success:
      return {
        ...state,
        error: false,
        isLoading: false,
        data: action.response,
        receivedAt: action.receivedAt || state.receivedAt,
      };
    case BooksCollectionGetAsyncActionTypes.failure:
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };
    default:
      return state;
  }
};
