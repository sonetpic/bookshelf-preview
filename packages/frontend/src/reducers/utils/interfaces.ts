import { ApiErrorActionInterface } from '../../middleware/api';

export interface AsyncActionStateInterface<Data> {
  data: Data;
  isLoading: boolean;
  error: ApiErrorActionInterface | false;
  receivedAt: number;
}
