import { combineReducers } from 'redux';
import { authorsReducer, AuthorsStateInterface } from './authors';
import { booksReducer, BooksStateInterface } from './books';
import { settingsReducer, SettingsStateInterface } from './settings';

export * from './authors';
export * from './books';

export interface RootStateInterface {
  books: BooksStateInterface;
  authors: AuthorsStateInterface;
  settings: SettingsStateInterface;
}

export const rootReducer = combineReducers<
  RootStateInterface
> ({
  books: booksReducer,
  authors: authorsReducer,
  settings: settingsReducer,
});
