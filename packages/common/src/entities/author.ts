import { Trim } from 'class-sanitizer';
import { IsNumber, Length, ValidateNested } from 'class-validator';
import { Column, Entity, Index, ManyToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Book, BookDto } from './book';

/**
 * Основные поля
 */
@Index(
  'name_sequence',
  [
    'firstName',
    'lastName',
  ],
  {
    unique: true,
  },
)
export class AuthorBasicDto {
  @Column({
    length: 20,
  })
  @Length(1, 20)
  @Trim()
  firstName!: string;

  @Column({
    length: 20,
  })
  @Length(1, 20)
  @Trim()
  lastName!: string;
}

/**
 * Описание с айди
 */
export class AuthorDto extends AuthorBasicDto {
  @IsNumber()
  id!: number;
}

/**
 * Описание с айди книг
 */
export class AuthorBasicWithBookIdsDto extends AuthorBasicDto {
  @IsNumber(
    {},
    {
      each: true,
    },
  )
  books!: Array<number>;
}

/**
 * Описание с айди и айди книг
 */
export class AuthorWithBookIdsDto extends AuthorBasicWithBookIdsDto {
  @IsNumber()
  id!: number;
}

/**
 * Описание с айди и книгами
 */
export class AuthorWithBooksDto extends AuthorBasicDto {
  @ManyToMany(
    type => Book,
    book => book.authors,
  )
  @ValidateNested()
  books!: Array<BookDto>;
}

/**
 * Полное описание сущности:
 * * основные поля
 * * айди
 * * книги
 */
@Entity()
export class Author extends AuthorWithBooksDto {
  @PrimaryGeneratedColumn()
  id!: number;
}
