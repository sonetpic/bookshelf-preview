import { Trim } from 'class-sanitizer';
import { ArrayNotEmpty, IsISBN, IsNumber, IsOptional, IsUrl, Length, Min, ValidateNested } from 'class-validator';
import { Column, Entity, JoinTable, ManyToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Author, AuthorDto } from './author';

/**
 * Основные поля
 */
export class BookBasicDto {
  @Column({
    length: 30,
  })
  @Length(1, 30)
  @Trim()
  title!: string;

  @Column({
    length: 13,
    unique: true,
  })
  @IsISBN()
  @Trim()
  isbn!: string;

  @Column({
    type: 'varchar',
    length: 256,
    nullable: true,
  })
  @IsUrl()
  @IsOptional()
  @Trim()
  image!: string | null;

  @Column({
    type: 'smallint',
    width: 4,
    nullable: true,
  })
  @Min(1800)
  @IsOptional()
  publicationYear!: number | null;
}

/**
 * Описание с айди
 */
export class BookDto extends BookBasicDto {
  @IsNumber()
  id!: number;
}

/**
 * Описание с айди авторов
 */
export class BookBasicWithAuthorIdsDto extends BookBasicDto {
  @IsNumber(
    {},
    {
      each: true,
    },
  )
  authors!: Array<number>;
}

/**
 * Описание с айди и айди авторов
 */
export class BookWithAuthorIdsDto extends BookBasicWithAuthorIdsDto {
  @IsNumber()
  id!: number;
}

/**
 * Описание с авторами
 */
export class BookBasicWithAuthorsDto extends BookBasicDto {
  @ManyToMany(
    type => Author,
    author => author.books,
  )
  @JoinTable()
  @ArrayNotEmpty()
  @ValidateNested()
  authors!: Array<AuthorDto>;
}

/**
 * Полное описание сущности:
 * * основные поля
 * * айди
 * * авторы
 */
@Entity()
export class Book extends BookBasicWithAuthorsDto {
  @PrimaryGeneratedColumn()
  id!: number;
}
