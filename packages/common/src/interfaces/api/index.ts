
export interface ApiErrorInterface {
  status: number;
  message: string;
  description?: string;
}

export type ApiDataInterface =
  Array<
    Object |
    number
  > |
  Object;

export type ApiSettingsInterface = Partial<{
  relations: Array<string>;
}>;

export interface ApiRemoveResponseDataInterface<
  List = Array<number>
> {
  resolved: List;
  rejected: List;
}
