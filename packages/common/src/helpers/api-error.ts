import hasProperty from 'ts-has-property';
import { ApiErrorInterface } from '../interfaces/api';

export class ApiError extends Error {
  status: number;
  message: string;
  description?: string;

  constructor(input: ApiErrorInterface) {
    super();
    this.status = input.status;
    this.message = input.message;
    this.description = input.description;
  }
}

export const isApiError = (
  data: unknown,
): data is ApiErrorInterface => {
  return hasProperty(data, 'message', 'string') &&
    hasProperty(data, 'status', 'number');
};
