import { IsNumber } from 'class-validator';
import { ApiRequest, ApiResponse } from '..';
import { ApiRemoveResponseDataInterface } from '../../interfaces/api';

export class RemoveAuthorsRequestDto extends ApiRequest<
  Array<number>
> {
  @IsNumber({}, {
    each: true,
  })
  data!: Array<number>;
}

export class RemoveAuthorsResponseDto extends ApiResponse<
  ApiRemoveResponseDataInterface
> {}
