import { IsNumber } from 'class-validator';
import { ApiRequest, ApiResponse } from '..';
import { Author, AuthorDto } from '../../entities';

export class GetAuthorsRequestDto extends ApiRequest<
  Array<number>
> {
  @IsNumber({}, {
    each: true,
  })
  data!: Array<number>;
}

export class GetAuthorsResponseDto extends ApiResponse<
  Array<AuthorDto | Author>
> {}
