import { ValidateNested } from 'class-validator';
import { ApiRequest, ApiResponse } from '..';
import { Author, AuthorDto } from '../../entities';

export class UpdateAuthorsRequestDto extends ApiRequest<
  Partial<AuthorDto>
> {
  @ValidateNested()
  data!: Partial<AuthorDto>;
}

export class UpdateAuthorsResponseDto extends ApiResponse<
  Partial<AuthorDto | Author>
> {}
