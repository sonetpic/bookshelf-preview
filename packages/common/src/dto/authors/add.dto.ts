import { ValidateNested } from 'class-validator';
import { ApiRequest, ApiResponse } from '..';
import { AuthorBasicDto, AuthorDto } from '../../entities';

export class AddAuthorsRequestDto extends ApiRequest<
  AuthorBasicDto
> {
  @ValidateNested()
  data!: AuthorBasicDto;
}

export class AddAuthorsResponseDto extends ApiResponse<
  AuthorDto
> {}
