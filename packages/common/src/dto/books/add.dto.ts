import { ValidateNested } from 'class-validator';
import { ApiRequest, ApiResponse } from '..';
import { Book, BookBasicWithAuthorIdsDto } from '../../entities';

export class AddBooksRequestDto extends ApiRequest<
  BookBasicWithAuthorIdsDto
> {
  @ValidateNested()
  data!: BookBasicWithAuthorIdsDto;
}

export class AddBooksResponseDto extends ApiResponse<
  Book
> {}
