import { IsNumber } from 'class-validator';
import { ApiRequest, ApiResponse } from '..';

export class RemoveBooksRequestDto extends ApiRequest<
  Array<number>
> {
  @IsNumber({}, {
    each: true,
  })
  data!: Array<number>;
}

export class RemoveBooksResponseDto extends ApiResponse<
  Array<number>
> {}
