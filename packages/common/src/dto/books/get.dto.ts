import { IsNumber } from 'class-validator';
import { ApiRequest, ApiResponse } from '..';
import { Book, BookDto } from '../../entities';

export class GetBooksRequestDto extends ApiRequest<
  Array<number>
> {
  @IsNumber({}, {
    each: true,
  })
  data!: Array<number>;
}

export class GetBooksResponseDto extends ApiResponse<
  Array<BookDto | Book>
> {}
