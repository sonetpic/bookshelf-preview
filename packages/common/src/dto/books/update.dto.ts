import { ValidateNested } from 'class-validator';
import { ApiRequest, ApiResponse } from '..';
import { BookWithAuthorIdsDto } from '../../entities';

export class UpdateBooksRequestDto extends ApiRequest<
  Partial<BookWithAuthorIdsDto>
> {
  @ValidateNested()
  data!: Partial<BookWithAuthorIdsDto>;
}

export class UpdateBooksResponseDto extends ApiResponse<
  Partial<BookWithAuthorIdsDto>
> {}
