import { IsDefined, IsOptional } from 'class-validator';
import { ApiDataInterface, ApiSettingsInterface } from '../interfaces/api';

export class ApiRequest<
  Data extends ApiDataInterface = {},
> {
  @IsDefined()
  data!: Data;

  @IsOptional()
  settings?: ApiSettingsInterface;
}

export class ApiResponse<
  Data extends ApiDataInterface = {},
> {
  @IsDefined()
  data!: Data;
}

// TODO: переписать DTO-шки в единый класс с `group` у полей,
// а структуру методов организовать через оверлоадинг интерфейсами

export * from './authors';
export * from './books';
