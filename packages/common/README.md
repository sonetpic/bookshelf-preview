# Bookshelf/Common

В данной папке собраны общие для [`backend`]-а и [`frontend`]-а ассеты. Это `shared`, который назван `common` (а ещё он мог иметь имя `middleware`, но этого не случилось).


<!-- LINKS -->
[`backend`]: ../backend/README.md
[`frontend`]: ../frontend/README.md
