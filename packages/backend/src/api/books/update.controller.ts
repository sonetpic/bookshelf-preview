import { Body, Controller, HttpCode, HttpStatus, Post } from '@nestjs/common';
import { UpdateBooksRequestDto, UpdateBooksResponseDto } from '@said-m/bookshelf-common/src/dto';
import { Author, Book } from '@said-m/bookshelf-common/src/entities';
import { ApiError } from '@said-m/bookshelf-common/src/helpers';
import { sanitize } from 'class-sanitizer';
import { plainToClass } from 'class-transformer';
import { validate } from 'class-validator';
import { isEmpty, isPlainObject, pick } from 'lodash';
import hasProperty from 'ts-has-property';
import { createQueryBuilder, getRepository } from 'typeorm';
import { getRelationIdList } from '../../common/helpers/get-relation-id-list.helper';
import { getFormattedIsbn } from './helpers';

@Controller()
export class UpdateBooksController {
  @Post('/update')
  @HttpCode(HttpStatus.OK)
  async update(
    @Body() body: UpdateBooksRequestDto,
  ): Promise<UpdateBooksResponseDto> {
    const data = body.data;

    const fields = [
      'title',
      'isbn',
      'image',
      'publicationYear',
    ] as const;

    // Проверка наличия объекта описания
    if (!isPlainObject(data)) {
      throw new ApiError({
        status: HttpStatus.BAD_REQUEST,
        message: 'Некорректный формат описания книги',
      });
    }

    // Проверка, что указан обновляемый объект
    if (!hasProperty(data, 'id', 'number')) {
      throw new ApiError({
        status: HttpStatus.BAD_REQUEST,
        message: 'Не указан обновляемый объект',
        description: 'Необходимо в описании передать `id` обновляемого объекта',
      });
    }

    const idToUpdate: number = data.id;

    // Изъятие только нужных полей
    const requestBook = pick(data, fields);

    // Получение списка авторов книги
    const formatedAuthors: Array<
      number
    > = hasProperty(data, 'authors', 'array')
      ? getRelationIdList(data.authors).map(
        thisAuthor => thisAuthor.id,
      )
      : [];

    const hasBookUpdates = !isEmpty(requestBook);
    const hasAuthorUpdates = formatedAuthors.length > 0;

    if (
      !hasBookUpdates &&
      !hasAuthorUpdates
    ) {
      throw new ApiError({
        status: HttpStatus.BAD_REQUEST,
        message: 'Не переданы значения для обновления',
      });
    }

    const isIdExists = await getRepository(Book).findOne(idToUpdate);
    if (!isIdExists) {
      throw new ApiError({
        status: HttpStatus.BAD_REQUEST,
        message: 'Обновление невозможно',
        description: `Книга с \`id\`: ${idToUpdate} - не существует`,
      });
    }

    // Обновление списка авторов
    if (hasAuthorUpdates) {
      const currentAuthors = await createQueryBuilder()
        .relation(Book, 'authors')
        .of(idToUpdate)
        .loadMany();

      const newAuthors = await getRepository(Author)
        .findByIds(formatedAuthors);

      await createQueryBuilder()
        .relation(Book, 'authors')
        .of(idToUpdate)
        .addAndRemove(newAuthors, currentAuthors);
    }

    // Подготовка записи
    const updateBook: Partial<Book> = plainToClass(Book, requestBook);

    sanitize(updateBook);
    // Проверка валидности записи
    const validationErrors = await validate(
      updateBook,
      {
        skipUndefinedProperties: true,
      },
    );
    const isValid = !validationErrors.length;

    if (!isValid) {
      throw new ApiError({
        status: HttpStatus.BAD_REQUEST,
        message: 'Некорректное описание книги',
        description: validationErrors.join('\n'),
      });
    }

    // Форматирование ISBN - только цифры
    if (updateBook.isbn) {
      updateBook.isbn = getFormattedIsbn(updateBook.isbn);
    }

    const bookRepository = await getRepository(Book);

    if (hasBookUpdates) {
      // Обновление данных книги
      const updated = await bookRepository.update(idToUpdate, updateBook);

      // Проверка наличия произведённого обновления
      if (!updated.affected) {
        throw new ApiError({
          status: HttpStatus.BAD_REQUEST,
          message: 'Не нашлось записи для обновления',
        });
      }
    }

    // Отправка результата клиенту
    return {
      data: {
        ...updateBook,
        id: idToUpdate,
        authors: formatedAuthors.length
          ? formatedAuthors
          : [],
      },
    };
  }
}
