import { Body, Controller, HttpCode, HttpStatus, Post } from '@nestjs/common';
import { GetBooksRequestDto, GetBooksResponseDto } from '@said-m/bookshelf-common/src/dto';
import { Book } from '@said-m/bookshelf-common/src/entities';
import { isArray } from '@said-m/common';
import { FindManyOptions, getRepository } from 'typeorm';

@Controller()
export class GetBooksController {
  @Post('/get')
  @HttpCode(HttpStatus.OK)
  async get(
    @Body() body: GetBooksRequestDto,
  ): Promise<GetBooksResponseDto> {
    const data = body.data;
    const settings = body.settings || {};

    const bookRepository = await getRepository(Book);
    const options: FindManyOptions<Book> = {
      relations: settings.relations,
    };

    const books = await (
      isArray(data) && data.length
        ? bookRepository.findByIds(data, options)
        : bookRepository.find(options)
    );

    return {
      data: books,
    };
  }
}
