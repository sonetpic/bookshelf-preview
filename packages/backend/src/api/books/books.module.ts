import { Module } from '@nestjs/common';
import { AddBooksController } from './add.controller';
import { GetBooksController } from './get.controller';
import { RemoveBooksController } from './remove.controller';
import { UpdateBooksController } from './update.controller';
import { BooksService } from './books.service';

@Module({
  controllers: [
    AddBooksController,
    GetBooksController,
    RemoveBooksController,
    UpdateBooksController,
  ],
  providers: [BooksService],
})
export class BooksModule {}
