import { Body, Controller, HttpStatus, Post } from '@nestjs/common';
import { AddBooksResponseDto } from '@said-m/bookshelf-common/src/dto';
import { Author, Book, BookBasicWithAuthorIdsDto } from '@said-m/bookshelf-common/src/entities';
import { ApiError } from '@said-m/bookshelf-common/src/helpers';
import { sanitize } from 'class-sanitizer';
import { plainToClass } from 'class-transformer';
import { validate } from 'class-validator';
import { isPlainObject, pick } from 'lodash';
import hasProperty from 'ts-has-property';
import { createQueryBuilder, getRepository } from 'typeorm';
import { getRelationIdList, RelationIdListInterface } from '../../common/helpers/get-relation-id-list.helper';
import { getFormattedIsbn } from './helpers';

@Controller()
export class AddBooksController {
  @Post('/add')
  async add(
    @Body('data') data: BookBasicWithAuthorIdsDto,
  ): Promise<AddBooksResponseDto> {

    // Проверка наличия объекта описания
    if (!isPlainObject(data)) {
      throw new ApiError({
        status: HttpStatus.BAD_REQUEST,
        message: 'Некорректный формат описания книги',
      });
    }

    const fields = [
      'title',
      'isbn',
      'image',
      'publicationYear',
    ] as const;

    // Изъятие только нужных полей
    const requestBook = pick(data, fields);

    // Получение списка авторов книги
    const formatedAuthors: Array<
      RelationIdListInterface
    > = hasProperty(data, 'authors', 'array')
      ? getRelationIdList(data.authors)
      : [];

    // Проверка наличия автора
    if (!formatedAuthors.length) {
      throw new ApiError({
        status: HttpStatus.BAD_REQUEST,
        message: 'Не передан информация об авторе',
        description: 'У книги должен быть хотя бы один автор',
      });
    }

    // Подготовка записи
    const newBook = plainToClass(Book, {
      ...requestBook,
      authors: formatedAuthors,
    });

    sanitize(newBook);
    // Проверка валидности записи
    const validationErrors = await validate(newBook);
    const isValid = !validationErrors.length;

    if (!isValid) {
      throw new ApiError({
        status: HttpStatus.BAD_REQUEST,
        message: 'Некорректное описание книги',
        description: validationErrors.join('\n'),
      });
    }

    // Форматирование ISBN - только цифры
    newBook.isbn = getFormattedIsbn(newBook.isbn);

    const bookRepository = await getRepository(Book);

    // Запись в базу
    const savedBook = await bookRepository.save(newBook);

    // Получаем список авторов
    const savedBookAuthors: Array<Author> = await createQueryBuilder()
      .relation(Book, 'authors')
      .of(savedBook)
      .loadMany();

    // Отправка результата клиенту
    return {
      data: {
        ...savedBook,
        authors: savedBookAuthors,
      },
    };
  }
}
