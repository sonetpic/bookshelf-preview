import { Body, Controller, HttpCode, HttpStatus, Post } from '@nestjs/common';
import { RemoveBooksRequestDto, RemoveBooksResponseDto } from '@said-m/bookshelf-common/src/dto';
import { Book } from '@said-m/bookshelf-common/src/entities';
import { ApiError } from '@said-m/bookshelf-common/src/helpers';
import { isArray, isEmpty, isNumber } from 'lodash';
import { getRepository } from 'typeorm';

@Controller()
export class RemoveBooksController {
  @Post('/remove')
  @HttpCode(HttpStatus.OK)
  async remove(
    @Body() body: RemoveBooksRequestDto,
  ): Promise<RemoveBooksResponseDto> {
    const data = body.data;

    // Проверка наличия объекта описания
    if (
      !isArray(data) ||
      isEmpty(data)
    ) {
      throw new ApiError({
        status: HttpStatus.BAD_REQUEST,
        message: 'Некорректный формат описания удаляемых элементов',
        description: 'Необходимо передать массив `id`-шников',
      });
    }

    // Для удаления требуются только `id`-шники
    const requestIds = data.filter(isNumber);

    const bookRepository = await getRepository(Book);

    // Запись в базу
    const deletedBooks = await bookRepository.delete(requestIds);

    if (!deletedBooks.affected) {
      throw new ApiError({
        status: HttpStatus.BAD_REQUEST,
        message: 'Не нашлось данных для удаления',
        description: 'Возможно, указанные объекты уже были удалены',
      });
    }

    // Отправка результата клиенту
    return {
      data: requestIds,
    };
  }
}
