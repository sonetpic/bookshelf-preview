
export const getFormattedIsbn = (value: string) => value.replace(/[^0-9]/g, '');
