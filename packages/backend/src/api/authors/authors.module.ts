import { Module } from '@nestjs/common';
import { AddAuthorsController } from './add.controller';
import { GetAuthorsController } from './get.controller';
import { RemoveAuthorsController } from './remove.controller';
import { UpdateAuthorsController } from './update.controller';

@Module({
  controllers: [
    AddAuthorsController,
    GetAuthorsController,
    RemoveAuthorsController,
    UpdateAuthorsController,
  ],
})
export class AuthorsModule { }
