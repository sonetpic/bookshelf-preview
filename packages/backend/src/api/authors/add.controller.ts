import { Body, Controller, HttpStatus, Post } from '@nestjs/common';
import { AddAuthorsRequestDto, AddAuthorsResponseDto } from '@said-m/bookshelf-common/src/dto';
import { Author, AuthorBasicDto } from '@said-m/bookshelf-common/src/entities';
import { ApiError } from '@said-m/bookshelf-common/src/helpers';
import { sanitize } from 'class-sanitizer';
import { plainToClass } from 'class-transformer';
import { validate } from 'class-validator';
import { isPlainObject, pick } from 'lodash';
import { getRepository } from 'typeorm';

@Controller()
export class AddAuthorsController {
  @Post('/add')
  async add(
    @Body() body: AddAuthorsRequestDto,
  ): Promise<AddAuthorsResponseDto> {
    const data = body.data;

    const fields = [
      'firstName',
      'lastName',
    ] as const;

    // Проверка наличия объекта описания
    if (!isPlainObject(data)) {
      throw new ApiError({
        status: HttpStatus.BAD_REQUEST,
        message: 'Некорректный формат описания автора',
      });
    }

    // Изъятие только нужных полей
    const requestAuthor = pick(data, fields);

    // Подготовка записи
    const newAuthor = plainToClass(AuthorBasicDto, requestAuthor);

    sanitize(newAuthor);
    // Проверка валидности записи
    const validationErrors = await validate(newAuthor);
    const isValid = !validationErrors.length;

    if (!isValid) {
      throw new ApiError({
        status: HttpStatus.BAD_REQUEST,
        message: 'Некорректное описание автора',
        description: validationErrors.join('\n'),
      });
    }

    const authorRepository = await getRepository(Author);

    // Запись в базу
    const savedAuthor = await authorRepository.save(newAuthor);

    // Отправка результата клиенту
    return {
      data: savedAuthor,
    };
  }
}
