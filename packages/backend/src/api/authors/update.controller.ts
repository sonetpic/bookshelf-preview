import { Body, Controller, HttpCode, HttpStatus, Post } from '@nestjs/common';
import { UpdateAuthorsRequestDto, UpdateAuthorsResponseDto } from '@said-m/bookshelf-common/src/dto';
import { Author } from '@said-m/bookshelf-common/src/entities';
import { ApiError } from '@said-m/bookshelf-common/src/helpers';
import { isPlainObject } from '@said-m/common';
import { sanitize } from 'class-sanitizer';
import { plainToClass } from 'class-transformer';
import { validate } from 'class-validator';
import { pick } from 'lodash';
import hasProperty from 'ts-has-property';
import { getRepository } from 'typeorm';

@Controller()
export class UpdateAuthorsController {
  @Post('/update')
  @HttpCode(HttpStatus.OK)
  async update(
    @Body() body: UpdateAuthorsRequestDto,
  ): Promise<UpdateAuthorsResponseDto> {
    const data = body.data;

    const fields = [
      'firstName',
      'lastName',
    ] as const;

    // Проверка наличия объекта описания
    if (!isPlainObject(data)) {
      throw new ApiError({
        status: HttpStatus.BAD_REQUEST,
        message: 'Некорректный формат описания автора',
      });
    }

    // Проверка, что указан обновляемый объект
    if (!hasProperty(data, 'id', 'number')) {
      throw new ApiError({
        status: HttpStatus.BAD_REQUEST,
        message: 'Не указан обновляемый объект',
        description: 'Необходимо в описании передать `id` обновляемого объекта',
      });
    }

    const idToUpdate: number = data.id;

    // Изъятие только нужных полей
    const requestAuthor = pick(data, fields);

    // Подготовка записи
    const updateAuthor = plainToClass(Author, requestAuthor);

    sanitize(updateAuthor);
    // Проверка валидности записи
    const validationErrors = await validate(
      updateAuthor,
      {
        skipUndefinedProperties: true,
      },
    );
    const isValid = !validationErrors.length;

    if (!isValid) {
      throw new ApiError({
        status: HttpStatus.BAD_REQUEST,
        message: 'Некорректное описание автора',
        description: validationErrors.join('\n'),
      });
    }

    const authorRepository = await getRepository(Author);

    // Обновление данных
    const updated = await authorRepository.update(idToUpdate, updateAuthor);

    // Проверка наличия произведённого обновления
    if (!updated.affected) {
      throw new ApiError({
        status: HttpStatus.BAD_REQUEST,
        message: 'Не нашлось записи для обновления',
      });
    }

    // Отправка обновлённых полей клиенту
    return {
      data: {
        ...updateAuthor,
        id: idToUpdate,
      },
    };
  }
}
