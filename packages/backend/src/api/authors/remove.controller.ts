import { Body, Controller, HttpCode, HttpStatus, Post } from '@nestjs/common';
import { RemoveAuthorsRequestDto, RemoveAuthorsResponseDto } from '@said-m/bookshelf-common/src/dto';
import { Author } from '@said-m/bookshelf-common/src/entities';
import { ApiError } from '@said-m/bookshelf-common/src/helpers';
import { isArray, isEmpty, isNumber } from 'lodash';
import { createQueryBuilder, getRepository } from 'typeorm';

@Controller()
export class RemoveAuthorsController {
  @Post('/remove')
  @HttpCode(HttpStatus.OK)
  async remove(
    @Body() body: RemoveAuthorsRequestDto,
  ): Promise<RemoveAuthorsResponseDto> {
    const data = body.data;

    // Проверка наличия объекта описания
    if (
      !isArray(data) ||
      isEmpty(data)
    ) {
      throw new ApiError({
        status: HttpStatus.BAD_REQUEST,
        message: 'Некорректный формат описания удаляемых элементов',
        description: 'Необходимо предать массив `id`-шников авторов',
      });
    }

    // Для удаления требуются только `id`-шники
    const idsToDelete = data.filter(isNumber);

    const countField = 'bookCount';
    const authorBooks = await createQueryBuilder<{
      id: number,
      [countField]: number,
    }>(Author, 'author')
      .select('author.id')
      .whereInIds(idsToDelete)
      .loadRelationCountAndMap(
        `author.${countField}`,
        'author.books',
      )
      .getMany();

    if (!idsToDelete) {
      throw new ApiError({
        status: HttpStatus.BAD_REQUEST,
        message: 'Не указаны объекты для удаления',
        description: 'Необходимо передать `id`-шники авторов',
      });
    }

    const removableStatus: Record<
      'true' | 'false',
      Array<number>
    > = {
      true: [],
      false: [],
    };

    authorBooks.forEach(
      thisAuthor => thisAuthor.bookCount === 0
        ? removableStatus.true.push(thisAuthor.id)
        : removableStatus.false.push(thisAuthor.id),
    );

    if (!removableStatus.true.length) {
      throw new ApiError({
        status: HttpStatus.BAD_REQUEST,
        message: 'Удаление не возможно',
        description: 'Вероятно, за автором числятся некоторые работы.\n' +
          'Или указанной записи не существует',
      });
    }

    const removed = await getRepository(Author)
      .delete(removableStatus.true);

    if (!removed.affected) {
      throw new ApiError({
        status: HttpStatus.BAD_REQUEST,
        message: 'Удаление не произведено',
        description: 'Возможно, указанные объекты уже были удалены',
      });
    }

    // Отправка результата клиенту
    return {
      data: {
        resolved: removableStatus.true,
        rejected: removableStatus.false,
      },
    };
  }
}
