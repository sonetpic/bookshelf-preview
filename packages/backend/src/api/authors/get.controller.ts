import { Body, Controller, HttpCode, HttpStatus, Post } from '@nestjs/common';
import { GetAuthorsRequestDto, GetAuthorsResponseDto } from '@said-m/bookshelf-common/src/dto';
import { Author } from '@said-m/bookshelf-common/src/entities';
import { isArray } from '@said-m/common';
import { FindManyOptions, getRepository } from 'typeorm';

@Controller()
export class GetAuthorsController {
  @Post('/get')
  @HttpCode(HttpStatus.OK)
  async get(
    @Body() body: GetAuthorsRequestDto,
  ): Promise<GetAuthorsResponseDto> {
    const data = body.data;
    const settings = body.settings || {};

    const authorRepository = await getRepository(Author);
    const options: FindManyOptions<Author> = {
      relations: settings.relations,
    };

    // FIXME: Relation * was not found, please check if
    // it is correct and really exist in your entity

    const authors = await (
      isArray(data) && data.length
        ? authorRepository.findByIds(data, options)
        : authorRepository.find(options)
    );

    return {
      data: authors,
    };
  }
}
