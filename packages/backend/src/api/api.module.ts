import { Module } from '@nestjs/common';
import { AuthorsModule } from './authors/authors.module';
import { BooksModule } from './books/books.module';

@Module({
  imports: [
    BooksModule,
    AuthorsModule,
  ],
})
export class ApiModule { }
