import { Routes } from 'nest-router';
import { AuthorsModule } from './authors/authors.module';
import { BooksModule } from './books/books.module';

export const apiRoutes: Routes = [
  {
    path: '/books',
    module: BooksModule,
  },
  {
    path: '/authors',
    module: AuthorsModule,
  },
];
