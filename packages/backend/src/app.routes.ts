import { Routes } from 'nest-router';
import { apiRoutes } from './api/api.routes';

export const appRoutes: Routes = [
  {
    path: '/api',
    children: apiRoutes,
  },
];
