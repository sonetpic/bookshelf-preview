import { isPlainObject } from '@said-m/common';
import { compact, isNumber } from 'lodash';
import hasProperty from 'ts-has-property';

export interface RelationIdListInterface {
  id: number;
}

export const getRelationIdList = (
  data: Array<unknown>,
): Array<RelationIdListInterface> => compact(
  data.map(
    (thisAuthor): RelationIdListInterface | undefined => {
      if (isNumber(thisAuthor)) {
        return {
          id: thisAuthor,
        };
      } else if (
        isPlainObject(thisAuthor) &&
        hasProperty(thisAuthor, 'id', 'number')
      ) {
        return {
          id: thisAuthor.id,
        };
      }
    },
  ),
);
