import { HttpStatus, ValidationError } from '@nestjs/common';
import { ApiError } from '@said-m/bookshelf-common/src/helpers';

export const validationExceptionFactory = (
  errors: Array<ValidationError>,
) => new ApiError({
  status: HttpStatus.BAD_REQUEST,
  message: 'Ошибка при проверке содержимого запроса',
  description: errors.toString(),
});
