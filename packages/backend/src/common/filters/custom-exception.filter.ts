import { ArgumentsHost, Catch, ExceptionFilter, HttpStatus } from '@nestjs/common';
import { ApiError } from '@said-m/bookshelf-common/src/helpers';
import { Response } from 'express';

@Catch()
export class CustomExceptionFilter implements ExceptionFilter {
  catch(exception: unknown, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();

    const defaultStatus = HttpStatus.INTERNAL_SERVER_ERROR;

    const error = exception instanceof ApiError
      ? exception
      : (
        exception instanceof Error
          ? new ApiError({
            status: defaultStatus,
            message: 'Ошибка сервера',
            description: exception.message,
          })
          : new ApiError({
            status: defaultStatus,
            message: 'Неизвестная серверная ошибка',
            description: 'Пожалуйста, свяжитесь с технической поддержкой сервиса' +
              ' и опишите ситуацию.\nСпасибо! :)',
          })
      );

    response
      .status(error.status)
      .json(error);
  }
}
