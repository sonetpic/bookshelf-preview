import { Module } from '@nestjs/common';
import { RouterModule } from 'nest-router';
import { ApiModule } from './api/api.module';
import { appRoutes } from './app.routes';

@Module({
  imports: [
    RouterModule.forRoutes(appRoutes),
    ApiModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule { }
