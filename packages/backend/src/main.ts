import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { createConnection } from 'typeorm';
import { AppModule } from './app.module';
import { CustomExceptionFilter, validationExceptionFactory } from './common';

createConnection().then(
  () => {
    console.log('Соединение с БД установлено');
    console.log('Запускаю сервер');

    async function bootstrap() {
      const app = await NestFactory.create(AppModule);
      app.enableCors();
      app.useGlobalFilters(new CustomExceptionFilter());
      app.useGlobalPipes(new ValidationPipe({
        exceptionFactory: validationExceptionFactory,
      }));
      await app.listen(8080);
    }
    bootstrap().catch(
      error => console.error('Ошибка при запуске сервера', error),
    );
  },
).catch(
  (error: Error) => console.error('Ошибка при подключении к БД:', error.message),
);
