
INSERT INTO book
  ("title", "isbn", "publicationYear", "image")
VALUES
  (
    'Асока',
    '9785389133228',
    2017,
    '/images/covers/Ahsoka.jpg'
  ),
  (
    'Траун',
    '9785389135901',
    2018,
    '/images/covers/Thrawn.jpg'
  ),
  (
    'Новый рассвет',
    '9785389135918',
    2017,
    '/images/covers/A_New_Dawn.jpg'
  ),
  (
    'Путь повстанца',
    '9785389139824',
    2018,
    '/images/covers/Rebel_Rising.jpg'
  ),
  (
    'Катализатор',
    '9785389125452',
    2017,
    '/images/covers/Catalyst.jpg'
  ),
  (
    'Изгой-Один',
    '9785389125469',
    2017,
    '/images/covers/Rogue_One.jpg'
  );
