
INSERT INTO author
  ("firstName", "lastName")
VALUES
  ('E. K.', 'Johnston'),
  ('Timothy', 'Zahn'),
  ('John Jackson', 'Miller'),
  ('Beth', 'Revis'),
  ('James', 'Luceno'),
  ('Alexander', 'Freed');
