
INSERT INTO book_authors_author
  ("authorId", "bookId")
VALUES
  (
    CURRVAL('"author_id_seq"'),
    CURRVAL('"book_id_seq"')
  ),
  (
    CURRVAL('"author_id_seq"') - 1,
    CURRVAL('"book_id_seq"') - 1
  ),
  (
    CURRVAL('"author_id_seq"') - 2,
    CURRVAL('"book_id_seq"') - 2
  ),
  (
    CURRVAL('"author_id_seq"') - 3,
    CURRVAL('"book_id_seq"') - 3
  ),
  (
    CURRVAL('"author_id_seq"') - 4,
    CURRVAL('"book_id_seq"') - 4
  ),
  (
    CURRVAL('"author_id_seq"') - 5,
    CURRVAL('"book_id_seq"') - 5
  );
