#!/bin/bash

# Заполняет тестовыми данным БД.
# Прежде, необходимо хотя бы раз запустить бэк,
# без ошибок... `TypeORM` создаст все необходимые таблицы.

# ENV

SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"

# SQL

cat \
  $SCRIPTPATH/../sql/insert-1-authors.sql \
  $SCRIPTPATH/../sql/insert-2-books.sql \
  $SCRIPTPATH/../sql/insert-3-book_authors.sql \
| psql -h localhost -U postgres -d bookshelf -p 5432 -a -q -f -
