#!/bin/bash

# Данный скрипт необходимо исполнить один раз,
# сразу после клонирования репозитория.

# ENV

SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"

# BASH

$SCRIPTPATH/bash/git.sh

# SQL

psql -h localhost -U postgres -p 5432 -a -q -f $SCRIPTPATH/sql/create-database.sql
