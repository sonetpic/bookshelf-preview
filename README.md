# Bookshelf

## Навигация

- [Bookshelf](#bookshelf)
  - [Навигация](#%d0%9d%d0%b0%d0%b2%d0%b8%d0%b3%d0%b0%d1%86%d0%b8%d1%8f)
  - [Начало](#%d0%9d%d0%b0%d1%87%d0%b0%d0%bb%d0%be)
    - [Кратко](#%d0%9a%d1%80%d0%b0%d1%82%d0%ba%d0%be)
    - [Подробнее](#%d0%9f%d0%be%d0%b4%d1%80%d0%be%d0%b1%d0%bd%d0%b5%d0%b5)
      - [Первый запуск](#%d0%9f%d0%b5%d1%80%d0%b2%d1%8b%d0%b9-%d0%b7%d0%b0%d0%bf%d1%83%d1%81%d0%ba)
        - [Начальная конфигурация](#%d0%9d%d0%b0%d1%87%d0%b0%d0%bb%d1%8c%d0%bd%d0%b0%d1%8f-%d0%ba%d0%be%d0%bd%d1%84%d0%b8%d0%b3%d1%83%d1%80%d0%b0%d1%86%d0%b8%d1%8f)
        - [Занесение тестовых данных](#%d0%97%d0%b0%d0%bd%d0%b5%d1%81%d0%b5%d0%bd%d0%b8%d0%b5-%d1%82%d0%b5%d1%81%d1%82%d0%be%d0%b2%d1%8b%d1%85-%d0%b4%d0%b0%d0%bd%d0%bd%d1%8b%d1%85)
      - [Запуск приложения](#%d0%97%d0%b0%d0%bf%d1%83%d1%81%d0%ba-%d0%bf%d1%80%d0%b8%d0%bb%d0%be%d0%b6%d0%b5%d0%bd%d0%b8%d1%8f)
        - [Полный](#%d0%9f%d0%be%d0%bb%d0%bd%d1%8b%d0%b9)
        - [Отдельный модуль](#%d0%9e%d1%82%d0%b4%d0%b5%d0%bb%d1%8c%d0%bd%d1%8b%d0%b9-%d0%bc%d0%be%d0%b4%d1%83%d0%bb%d1%8c)
          - [common](#common)
          - [backend](#backend)
          - [frontend](#frontend)
  - [Введение](#%d0%92%d0%b2%d0%b5%d0%b4%d0%b5%d0%bd%d0%b8%d0%b5)
    - [Скрипты](#%d0%a1%d0%ba%d1%80%d0%b8%d0%bf%d1%82%d1%8b)
      - [Lerna](#lerna)
      - [Root](#root)
      - [Backend](#backend)
      - [Common](#common)
      - [Frontend](#frontend)
    - [Читать дальше](#%d0%a7%d0%b8%d1%82%d0%b0%d1%82%d1%8c-%d0%b4%d0%b0%d0%bb%d1%8c%d1%88%d0%b5)
  - [Автор](#%d0%90%d0%b2%d1%82%d0%be%d1%80)
  - [Лицензия](#%d0%9b%d0%b8%d1%86%d0%b5%d0%bd%d0%b7%d0%b8%d1%8f)

## Начало

> **ℹ**: Если вы используйте `Windows` в качестве ОС, то рекомендуется использовать `WSL`. Установить подсистему можно прям из `Microsoft Store`.
> Более подробную информацию о том как установить и настроить - найдёте сами :)

### Кратко

```bash
yarn                      # устанавливаем все зависимости
./scripts/init.sh         # настройка `git` и `postgres`
yarn start                # запускаем приложение
./scripts/bash/mockup.sh  # заносим данные в БД
```

### Подробнее

Данный проект содержит в себе как `backend`, так `frontend`, а ещё и `common`. Живут они вместе, но разделяют ответственность, поэтому каждый из них имеет свой собственный `package.json`: [`packages/backend/package.json`], [`packages/common/package.json`] и [`packages/frontend/package.json`]. Если кому-то из них требуется `dependencies` (`dev`-, `peer`-, `optional`-* - не важно), то добавляется он только в соответствующей `package.json`, а не в `root`-овый.

#### Первый запуск

Прежде всего, необходимо установить зависимости:
```bash
yarn
```

Данная команда установить все необходимые зависимости из каждого `package.json` на нужном уровне.

##### Начальная конфигурация

Данный проект использует БД. Перед тем, как исполнить следующий скрипт, необходимо установить `Postgres`.

Помимо этого, необходимо сконфигурировать `Git` определённым образом.

```bash
./scripts/init.sh
```

##### Занесение тестовых данных

Проект использует `TypeORM` для работы с БД. Данный модуль создаст всё необходимое окружение для коректной работы с хранилищем. Нужно лишь его запустить. Поднимает `backend`:
```bash
yarn back start
```

После этого, можно использовать `SQL`-скрипты, которые заносят в таблицы БД все необходимые данные. Для удобства - выпольнения одной командой, воспользуемся `.sh`-шником:
```bash
./scripts/bash/mockup.sh
```

#### Запуск приложения

##### Полный

Чтобы запустить приложение полноценно, необходимо чтобы были запущены все его модули. Сделать это можно так:
```bash
yarn start
```

##### Отдельный модуль

###### `common`

Поставляет общий функционал для `backend` и `frontend`. Так как он используется в зависимостях указанных модулей, необходимо иметь собранную версию проекта. Чтобы уж точно не потерять внесённые изменения в файлы данного модуля, рекомендуется использовать следующую комманду:
```bash
yarn back run start
```

Данная команда собирает и пересобирает проект, если в него были внесены изменения.
Го дальше!

###### `backend`

Необходимо стартовать за тем, что он поставляет API для манипуляции над данными `frontend`-ом.
```bash
yarn back run start
```

Можно ограничиваться запуском только бэка, если не требуется правок в UI, т.е. их скоуп - API.

###### `frontend`

Клиент запускается следующим образом:
```bash
yarn front run start
```

Для полноценной работы клиента, ему нужен рабочий API, т.е. необходимо иметь запущенным `backend`.

После успешного выполнения всех команд, можно чекать приложение в браузере.

## Введение

> **⚠**: Ознакомьтесь с разделом [Подробнее](#Подробнее)

### Скрипты

Для того, чтобы выполнить скрипт для `frontend` или `backend`, необходимо сделать следующее:
```bash
yarn back run $СКРИПТ_БЭКА
# or
yarn front run $СКРИПТ_ФРОНТА
```

#### Lerna

Данный проект - моно-репозиторий, поэтому, для упрощения некоторых моментов, связанных с использование данной организации структуры, используется `Lerna`.

> **ℹ**: с версии `0.0.3`

Скрипты организованы таким образом, чтобы максимально нивелировать упоминания `Lerna`, однако о его (модуля) участии следует помнить.

#### Root

* `start` - запуск всех модулей
* `back` - выполнение `yarn` в контексте `backend`
* `common` - выполнение `yarn` в контексте `common`
* `front` - выполнение `yarn` в контексте `frontend`
* `lerna` - выполнение локального `lerna`, без установки в глобальный регистр или `npx`
* `back:add` - добавление зависимости для `backend`
* `common:add` - добавление зависимости для `common`
* `front:add` - добавление зависимости для `frontend`
* `lint` - линтинг кода
* `build` - сборка всех модулей
* `validate` - валидация кода - компиляция без вывода
* `release` - подготовка новой версии

> **⚠**: В корневом [`package.json`] не должно быть никаких `dependencies`.

#### Backend

* `start` - это то, что нужно
* `validate`
* `build`

<details>
  <summary>
    Примеры
  </summary>

  * Запуск сервера:
    ```bash
    yarn back start
    ```
  * Добавление зависимости:
    ```bash
    yarn back:add nodemon -dev
    ```
    в данном примере добавляются `nodemon` в `devDependencies`;
  * Удаление зависимостей:
    ```bash
    yarn back remove ejs babel
    ```
    в примере демонстрируется удаление зависимостей: `ejs` и `babel`;
</details>

#### Common

* `start` - это то, что нужно
* `validate`
* `clean`

#### Frontend

* `start` - это то, что нужно
* `validate`
* `build`

<details>
  <summary>
    Примеры
  </summary>

  * Запуск клиента:
    ```bash
    yarn front start
    ```
  * Сборка фронта:
    ```bash
    yarn front build
    ```
  * Добавление зависимостей:
    ```
    yarn front:add normalizr
    ```
    в примере демонстрируется добавление зависимости `normalizr`;
  * Удаление зависимостей:
    ```
    yarn front remove jquery
    ```
    в примере демонстрируется удаление зависимости `jquery`.
</details>

### Читать дальше

* [`packages/backend/README.md`]
* [`packages/common/README.md`]
* [`packages/frontend/README.md`]

## Автор

Саид Магомедов:
* [GitHub]
* [NPM]
* [VK]

## Лицензия

Содержимое данного проекта распространяется по [`MIT License`].

<!-- LINKS -->
<!-- assets -->
<!-- paths -->
[`MIT License`]: LICENSE
[`package.json`]: package.json
[`packages/backend/package.json`]: packages/backend/package.json
[`packages/common/package.json`]: packages/common/package.json
[`packages/frontend/package.json`]: packages/frontend/package.json
[`packages/backend/README.md`]: packages/backend/README.md
[`packages/common/README.md`]: packages/common/README.md
[`packages/frontend/README.md`]: packages/frontend/README.md
<!-- accounts -->
[GitHub]: https://github.com/said-m
[NPM]: https://www.npmjs.com/~said-m
[VK]: https://vk.com/id266788473
<!-- external -->
